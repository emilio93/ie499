% --------------------
  \chapter{Desarrollo}

% --------------------
\label{C:desarrollo}

\section{Implementación de RISC-V PicoRV32}

La implementación utilizada para el proyecto se llama PicoRV32, como su nombre lo indica, es de 32 bits, puede ser configurado para funcionar como RV32IM, RV32IC y RV32IMC, es decir, implementa extensiones para trabajar con enteros, multiplicación, división e instrucciones comprimidas. Además de esto tiene otra gran cantidad de parámetros que permiten un diseño que se adapte a las necesidades de distintos proyectos.

\subsection{Justificación de Elección}

Esta implementación es una de muchas que existen. Una gran cantidad de implementaciones son como PicoRV32, de código abierto, es decir, permiten al usuario revisar el código utilizado para la implementación. PicoRV32 utiliza la licencia ISC que permite además la copia, modificación y distribución del código, sea que se cobre o no por este. El código se provee sin ninguna clase de garantía, liberando al autor de cualquier responsabilidad, pero como se verá más adelante el proyecto cuenta con pruebas que permiten corroborar la funcionalidad del procesador.

Otro aspecto de importancia a la hora de considerar el proyecto es el lenguaje utilizado, Verilog. Este lenguaje es utilizado en cursos de la carrera, esto permite a los estudiantes concentrarse principalmente en la lógica del diseño. El proyecto cuenta con una gran cantidad de ejemplos y ha sido adaptado a múltiples plataformas, Vivado entre ellas, facilitando la adaptación del proyecto a las necesidades del curso de Laboratorio de Circuits Digitales.

Se trata de un diseño configurable, dando la opción de agregar extensiones para instrucciones comprimidas, multiplicación y división de enteros, y una gran cantidad de parámetros para ajustar el diseño a los requerimientos de quien lo utilice. Cuenta con interfaces de memoria e interrupciones e implementaciones para estas partes del diseño, sin embargo también permite realizar implementaciones propias si se desea. Otro aspecto configurable es el \textit{debugging} que muestra información detallada del procesador al ser simulado, esto es de especial utilidad cuando se realizan cambios en el procesador.

En general es un proyecto bien documentado y listo para ser utilizado, se considera apto para el desarrollo de este trabajo y es aplicable a una gran cantidad de situaciones. El sitio web de RISC-V lista otras implementaciones que pueden resultar más adecuadas ante situaciones específicas, es adecuado evaluar otras opciones si es el caso.

\subsection{Revisión del Proyecto}

La gran parte de la funcionalidad del procesador se encuentra en un único archivo que implementa el ISA, \texttt{picorv32.v}, este cuenta la implementación de la base RV32I, agrega módulos para la multiplicación y división, tiene una implementación simple de los registros y una versión con interfaz AXI4-Lite para memorias.

El proyecto también cuenta con una serie de agregados como pruebas, código para probar funcionalidad del procesador, una implementación para una tarjeta de desarrollo con el código correspondiente, y la implementación en múltiples ambientes de desarrollo.

El apéndice \ref{apendice:diagramas} muestra diagramas de las señales del procesador y sus conexiones al ser utilizado en la tarjeta de desarrollo.


\subsection{Compilación de Código a Ejecutar}

Se utilizan las herramientas provistas por RISC-V en su versión para 32 bits, estas permiten compilar código C, C++ entre otras tareas, la instalación de estas herramientas se detalla en el archivo readme del proyecto PicoRV32
\footnote{Específicamente en
\url{https://github.com/cliffordwolf/PicoRV32\#building-a-pure-rv32i-toolchain}
}
y son derivadas de las instrucciones originales para la base de 64 bits, si bien es un proceso que puede tomar tiempo, no es uno difícil de seguir.
Al seguir este procedimiento se habilitan los siguientes ejecutables:

\begin{minted}{text}
riscv32-unknown-elf-addr2line     riscv32-unknown-elf-gcc
riscv32-unknown-elf-gdb           riscv32-unknown-elf-readelf
riscv32-unknown-elf-ar            riscv32-unknown-elf-gcc-8.2.0
riscv32-unknown-elf-gprof         riscv32-unknown-elf-run
riscv32-unknown-elf-as            riscv32-unknown-elf-gcc-ar
riscv32-unknown-elf-ld            riscv32-unknown-elf-size
riscv32-unknown-elf-c++           riscv32-unknown-elf-gcc-nm
riscv32-unknown-elf-ld.bfd        riscv32-unknown-elf-strings
riscv32-unknown-elf-c++filt       riscv32-unknown-elf-gcc-ranlib
riscv32-unknown-elf-nm            riscv32-unknown-elf-strip
riscv32-unknown-elf-cpp           riscv32-unknown-elf-gcov
riscv32-unknown-elf-objcopy       riscv32-unknown-elf-elfedit
riscv32-unknown-elf-gcov-dump     riscv32-unknown-elf-objdump
riscv32-unknown-elf-g++           riscv32-unknown-elf-gcov-tool
riscv32-unknown-elf-ranlib
\end{minted}

Si bien todas estas herramientas tienen utilidad, nos interesan \texttt{riscv32-unknown-elf-gcc} y \texttt{riscv32-unknown-elf-objdump}, la primera compila código C en código máquina para RISC-V, el segundo desensambla un archivo compilado en las instrucciones en ensamblador correspondientes al programa.

Se toma para compilar con \texttt{riscv32-unknown-elf-gcc} el código en el directorio \texttt{firmware/} en la raíz del proyecto se puede utilizar las siguientes reglas de \texttt{make} en la raíz del proyecto.

\begin{minted}{bash}
  # crear el ejecutable principal a partir del
  # código ensamblador firmware/start.S
  make firmware/start.o

  # compilar funciones en archivos .c y obtener archivo .elf
  # este toma la posición de sus secciones del archivo
  # firmware/sections.lds
  make firmware/firmware.elf

  # obtener archivos en binario y hexadecimal del ejecutables
  make firmware/firmware.bin
  make firmware/firmware.hex
\end{minted}

En una implementación sencilla, el archivo hexadecimal es cargado en la memoria de instrucciones, el archivo es generado por un \textit{script} que asegura que el archivo tiene un tamaño especificado por un argumento al \textit{script}, este valor debe coincidir con el tamaño de la memoria en el diseño.

Teniendo un archivo \texttt{.elf} se puede desensamblar en las instrucciones de ensamblador correspondientes, para esto se ejecutaría:

\begin{minted}{bash}
  # desde la raíz del proyecto
  riscv32-unknown-elf-objdump -d firmware/firmware.elf
\end{minted}

Para cada ejecutable instalado se puede realizar un llamado con la bandera \texttt{-}\texttt{-help} que indica el uso del programa asi como las banderas que pueden ser utilizadas en este.

Es muy probable que no se pueda ejecutar el código en la computadora, esto porque el ISA de la mayoría de computadoras actualmente no es RISC-V, las opciones que se tienen son simular un diseño del procesador al cual se le provea el código compilado o sintetizar un diseño para probar el código en este, a continuación se exponen ambas maneras.

\subsection{Simulación de Comportamiento del Sistema}

Se realiza la simulación del mismo código dado para corroborar que el sistema esté funcionando de manera adecuada.

Se puede ejecutar la prueba para el código compilado con la siguiente regla \texttt{make}:

\begin{minted}{bash}
  # correr las pruebas
  make test
\end{minted}

Esto ejecutará lo necesario para correr la prueba, incluyendo la compilación del código en caso de no haber sido realizada. El \textit{software} utilizado para correr la prueba es Icarus Verilog.

La prueba consta del conjunto de pruebas que brinda RISC-V\footnote{Disponible en: \url{https://github.com/riscv/riscv-tests}}, una criba para la obtención de números primos entre otras que corroboran el correcto funcionamiento del diseño. Este código es de importancia porque permite verificar la concordancia con el ISA al realizar cambios en la implementación.

Estas son las pruebas que se realizan desde el directorio base, el directorio \texttt{dhrystone} contiene código para ejecutar un \textit{benchmark}, osea, un código que busca determinar el rendimiento de un procesador, en este caso de la implementación en PicoRV32. En el directorio \texttt{scripts} se encuentran múltiples flujos de diseño para distintas herramientas y arquitecturas de \textit{hardware}, se incluye un ejemplo para el ambiente Vivado, dirigido a tarjetas de desarrollo con \textit{FPGAs} en la línea \textit{7-Series} de Xilinx. El código utilizado para este ejemplo se puede correr de la siguiente manera:

\begin{minted}{bash}
  # sintetizar el diseño
  make synth_system

  # ejecutar simulación|
  make sim_system
\end{minted}

Imprime el siguiente texto:

\begin{minted}{text}
Hello World! If you can read this message then
the PicoRV32 CPU seems to be working just fine.

                TEST PASSED!
\end{minted}
