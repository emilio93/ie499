\section{RISC-V}

RISC-V es un proyecto que surgió en la Universidad de California, Berkeley en el año 2010 como un diseño para un curso de diseño de sistemas VLSI. El proyecto creció, con esto vio la necesidad de crear una fundación para mantener el set de instrucciones RISC-V, esto se hizo en el año 2015, dado que múltiples empresas utilizaban el proyecto en sus productos\cite{misc:aboutriscvfoundation}.

En si RISC-V es una especificación y no una implementación, el hecho de ser una arquitectura creada desde cero permite evitar problemas heredados de una arquitectura más vieja como x86 o ARM\cite{misc:riscvqemupres}. Se cuenta con un diseño modular en torno a un modulo base, se permiten sistemas de 32, 64 y hasta 128 bits, permitiendo aplicar la especificación en una gran cantidad de productos. Se proveen 4 posibles módulos base, RV32I, RV32E, RV64I y RV128I, además se ofrece 13 extensiones para conjuntos de instrucciones o tareas relacionadas.

\subsection{La Especificación}

  La especificación de la arquitectura RISC-V\footnote{Se habla de \textit{The RISC-V Instruction Set Manual
  Volume I: User-Level ISA }\cite{Waterman14therisc}} se encuentra en su versión 2.2 desde mayo del 2017, y contiene las cuatro bases y las 13 extensiones, sin embargo es un documento que no está ratificado por la fundación. Hay bases o extensiones congeladas, es decir, no se espera que sufran cambios. Este apartado se centra en la base de 32 bits.

  Para identificar la longitud de la instrucción, en una arquitectura de 32 bits se puede tener instrucciones de 32 o 16 bits, para poder diferenciar, se utilizan los dos primeros bits de la instrucción, cuando el valor no es $(11)_2$ se trata de instrucciones de 16 bits. La figura \ref{img:riscv:insnenc} muestra las posibles codificaciones, para la base de 32 bits aplica únicamente las dos primeras codificaciones.

  \begin{figure}
    \centering
    \caption{Codificación de longitud de la instrucción. Obtenido de \cite{Waterman14therisc}.}
    \label{img:riscv:insnenc}
    \trule
    \resizebox{\textwidth}{!}{
    \begin{tabular}{ccccl}
      \cline{4-4}
      & & & \multicolumn{1}{|c|}{\texttt{xxxxxxxxxxxxxxaa}} & 16-bit ({\texttt{aa}}
      $\neq$ {\texttt{11}})\\
      \cline{4-4}
      \\
      \cline{3-4}
      & & \multicolumn{1}{|c|}{\texttt{xxxxxxxxxxxxxxxx}}
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxxxxbbb11}} & 32-bit ({\texttt{bbb}}
      $\neq$ {\texttt{111}}) \\
      \cline{3-4}
      \\
      \cline{2-4}
      \hspace{0.1in}
      & \multicolumn{1}{c|}{$\cdot\cdot\cdot${\texttt{xxxx}} }
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxxxxxxxxx}}
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxxx011111}} & 48-bit \\
      \cline{2-4}
      \\
      \cline{2-4}
      \hspace{0.1in}
      & \multicolumn{1}{c|}{$\cdot\cdot\cdot${\texttt{xxxx}} }
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxxxxxxxxx}}
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxx0111111}} & 64-bit \\
      \cline{2-4}
      \\
      \cline{2-4}
      \hspace{0.1in}
      & \multicolumn{1}{c|}{$\cdot\cdot\cdot${\texttt{xxxx}} }
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxxxxxxxxx}}
      & \multicolumn{1}{c|}{\texttt{xnnnxxxxx1111111}} & (80+16*{\texttt{nnn}})-bit,
             {\texttt{nnn}}$\neq${\texttt{111}} \\
      \cline{2-4}
      \\
      \cline{2-4}
      \hspace{0.1in}
      & \multicolumn{1}{c|}{$\cdot\cdot\cdot${\texttt{xxxx}} }
      & \multicolumn{1}{c|}{\texttt{xxxxxxxxxxxxxxxx}}
      & \multicolumn{1}{c|}{\texttt{x111xxxxx1111111}} & Reserved for $\geq$192-bits \\
      \cline{2-4}
      \\
      Byte Address: & \multicolumn{1}{r}{base+4} & \multicolumn{1}{r}{base+2} & \multicolumn{1}{r}{base} & \\
      \end{tabular}
    }
    \brule
  \end{figure}

  La base de 32 bits cuenta con 31 registros de uso general, a saber \texttt{x1-x31}, el registro \texttt{x0} corresponde a un valor fijado de cero. La figura \ref{gprs} los muestra, además de estos existe el \texttt{pc}, un registro que indica la dirección de la instrucción actual.

  \begin{figure}
    \centering
    \caption{Convenciones para registros de enteros en RISC-V. Adaptado de \cite{Waterman14therisc}.}
    \label{gprs}
    \trule
    {\footnotesize
    \begin{tabular}{|l|l|l|l|}
      \hline
      Register                      & ABI Name                       & Description                       & Saver \\ \hline
      {\texttt{x0}}                 & {\texttt{zero}}                & Hard-wired zero                   & --- \\
      {\texttt{x1}}                 & {\texttt{ra}}                  & Return address                    & Caller \\
      {\texttt{x2}}                 & {\texttt{sp}}                  & Stack pointer                     & Callee \\
      {\texttt{x3}}                 & {\texttt{gp}}                  & Global pointer                    & --- \\
      {\texttt{x4}}                 & {\texttt{tp}}                  & Thread pointer                    & --- \\
      {\texttt{x5}}                 & {\texttt{t0}}                  & Temporary/alternate link register & Caller \\
      {\texttt{x6}}--{\texttt{7}}   & {\texttt{t1}}--{\texttt{2}}    & Temporaries                       & Caller \\
      {\texttt{x8}}                 & {\texttt{s0}}/{\texttt{fp}}    & Saved register/frame pointer      & Callee \\
      {\texttt{x9}}                 & {\texttt{s1}}                  & Saved register                    & Callee \\
      {\texttt{x10}}--{\texttt{11}} & {\texttt{a0}}--{\texttt{1}}    & Function arguments/return values  & Caller \\
      {\texttt{x12}}--{\texttt{17}} & {\texttt{a2}}--{\texttt{7}}    & Function arguments                & Caller \\
      {\texttt{x18}}--{\texttt{27}} & {\texttt{s2}}--{\texttt{11}}   & Saved registers                   & Callee \\
      {\texttt{x28}}--{\texttt{31}} & {\texttt{t3}}--{\texttt{6}}    & Temporaries                       & Caller \\
      \hline
     \end{tabular}
    }
    \brule
  \end{figure}

  La figura \ref{riscv:insn_types} muestra los cuatro tipos de instrucciones disponibles para la base de 32 bits, es posible notar que las posiciones de los bits para registros de fuentes (rs1, rs2) y destino (rd) se mantienen en todos los tipos. Existen algunos tipos adicionales para codificar valores inmediatos\cite{Waterman14therisc}.

  \begin{figure}
    \centering
    \caption{Tipos de instrucciones en RISC-V. Adaptado de \cite{Waterman14therisc}.}
    \label{riscv:insn_types}
    \trule
    \setlength{\tabcolsep}{4pt}
    \begin{tabular}{p{1.2in}@{}p{0.8in}@{}p{0.8in}@{}p{0.6in}@{}p{0.8in}@{}p{1in}l}
    \\
    31\hfill 25 &
    24\hfill 20 &
    19\hfill 15 &
    14\hfill 12 &
    11\hfill 7 &
    6\hfill 0 \\
    \cline{1-6}
    \multicolumn{1}{|c|}{funct7} &
    \multicolumn{1}{c|}{rs2} &
    \multicolumn{1}{c|}{rs1} &
    \multicolumn{1}{c|}{funct3} &
    \multicolumn{1}{c|}{rd} &
    \multicolumn{1}{c|}{opcode} &
    tipo R \\
    \cline{1-6}
    \\
    \cline{1-6}
    \multicolumn{2}{|c|}{imm[11:0]} &
    \multicolumn{1}{c|}{rs1} &
    \multicolumn{1}{c|}{funct3} &
    \multicolumn{1}{c|}{rd} &
    \multicolumn{1}{c|}{opcode} &
    tipo I \\
    \cline{1-6}
    \\
    \cline{1-6}
    \multicolumn{1}{|c|}{imm[11:5]} &
    \multicolumn{1}{c|}{rs2} &
    \multicolumn{1}{c|}{rs1} &
    \multicolumn{1}{c|}{funct3} &
    \multicolumn{1}{c|}{imm[4:0]} &
    \multicolumn{1}{c|}{opcode} &
    tipo S \\
    \cline{1-6}
    \\
    \cline{1-6}
    \multicolumn{4}{|c|}{imm[31:12]} &
    \multicolumn{1}{c|}{rd} &
    \multicolumn{1}{c|}{opcode} &
    tipo U \\
    \cline{1-6}
    \end{tabular}
    \brule
  \end{figure}

  En general la especificación es amena con el lector resultando una excelente referencia para asuntos técnicos de la arquitectura.
