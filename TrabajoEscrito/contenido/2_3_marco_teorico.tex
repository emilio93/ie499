
\section{Unidad Central de Procesamiento, Procesador}

  La figura \ref{img:cpu:diagrama} muestra un esquema de una unidad de procesamiento sencilla. Lo que se hace es tener una serie de instrucciones en una memoria. Cada instrucción puede realizar una de un conjunto de operaciones disponibles. Las instrucciones se componen de la operación que se realiza y posiblemente una serie de argumentos. Las operaciones suelen agruparse en distintos tipos. Estas instrucciones son
  leídas por la interfaz con memoria del procesador y se pasan al buscador de instrucciones, donde se determina el tipo de instrucción así como los argumentos disponibles.

  El funcionamiento se desarrolla de la siguiente manera: se lee de una memoria las instrucciones, las cuales son decodificadas para determinar el tipo de operación y sus argumentos. Los registros necesarios para esta operación son pasados a la unidad lógica aritmética (ALU) que se encarga de realizar la operación en sí y devolver la salida, sea a los registros nuevamente o a memoria.

    \begin{figure}
      \centering
      \caption{Diagrama básico de un procesador. Adaptado de \href{https://commons.wikimedia.org/wiki/File:CPU_block_diagram.svg}{Wikimedia Commons}.}
      \label{img:cpu:diagrama}
      \trule
      \includegraphics[width=.4\textwidth]{imagenes/CPU_block_diagram.pdf}
      \brule
    \end{figure}

    En la figura \ref{img:cpu:riscvmachine} se muestra un diagrama más completo de un procesador RISC-V.

    \begin{figure}
      \centering
      \caption{Diagrama de un procesador RISC-V. Obtenido de \cite{book:Patterson2017}.}
      \label{img:cpu:riscvmachine}
      \trule
      \includegraphics[width=.8\textwidth]{imagenes/risc-v-machine.pdf}
      \brule
    \end{figure}

  \subsection{\textit{Pipeline}}

  En la figura \ref{img:cpu:riscvmachine} se observan rectángulos sombreados, los cuales identifican las etapas del \textit{pipeline}. Al ejecutar una instrucción se debe realizar una serie de pasos, obtener y decodificar la instrucción, obtener los registros necesarios, realizar la operación y guardar los resultados. Estos son posibles pasos, sin embargo se puede tener más o menos pasos en un \textit{pipeline} dependiendo de la implementación. Si bien se requiere múltiples ciclos de reloj para completar todos los pasos, se puede ejecutar todos los pasos en cada ciclo de reloj, de esta manera se debe esperar inicialmente que se ejecuten todos los pasos, pero una vez realizada la primer instrucción, las demás estarán completas cada ciclo de reloj. Por esto la utilidad de tener un \textit{pipeline} en el diseño.

  La figura \ref{img:pipeline} muestra una visualización del \textit{pipeline}, cada CC es un ciclo, las etapas mostradas son \textit{fetch}, cuando se obtiene la instrucción. \textit{Decode}, cuando se determina la operación y se solicitan los datos que correspondan. \textit{Execution}, cuando se realiza la operación. \textit{Memory access} en caso que se interactúe con la memoria. Por último, \textit{write back}, cuando se almacena el resultado de la operación en registros\cite{book:Patterson2017}. A partir del quinto ciclo de reloj se completan instrucciones cada ciclo de reloj.

  \begin{figure}
    \centering
    \caption{Visualización del pipeline. Obtenido de \cite{book:Patterson2017}.}
    \label{img:pipeline}
    \trule
    \resizebox{\textwidth}{!}{
      \includegraphics{imagenes/pipeline.pdf}
    }
    \brule
  \end{figure}

  \subsection{Jerarquía de Memorias}

  La información que es procesada debe ser almacenada de una u otra manera. Sea que se tenga cierto valor en un registro, sea almacenado en memoria cache o deba ser obtenido o almacenado en memoria principal. Para almacenar datos inclusive cuando no haya alimentación se utilizan tecnologías no volátiles como los discos duros o las unidades de estado sólido\cite{book:Patterson2017}.

  La figura \ref{img:mem_hier:mem_hier} muestra la jerarquía de memoria que se encuentra en distintos dispositivos así como los tamaños de las memorias y los tiempos de acceso a los datos. En todos los casos es notable el hecho que entre mayor capacidad se tiene mayor tiempo de acceso. Una memoria ideal debe tener un tiempo de acceso instantáneo y una capacidad de almacenamiento infinito. Como se notó, existe un compromiso entre estos dos elementos y es por este hecho que se tiene una jerarquía de memoria, en la que se plantea tener los datos más utilizados cerca del procesador, mientras que datos poco utilizados se almacenen en unidades con más capacidad. De esta manera se logra mejorar el tiempo de acceso promedio a los datos.

  \begin{figure}
    \centering
    \caption{Jerarquías de memoria en distintos dispositivos. Obtenido de \cite{book:Patterson2017}.}
    \label{img:mem_hier:mem_hier}
    \trule
    \includegraphics[width=.8\textwidth]{imagenes/mem_hier.pdf}
    \brule
  \end{figure}

  Los registros se encuentran dentro del procesador por lo que el dato puede ser obtenido en el siguiente ciclo de reloj.

  El cache es la siguiente memoria en la cadena de acceso. Se trata de una memoria rápida que almacena los datos que han sido solicitado previamente. En caso que se llene la memoria, el cache implementa un algoritmo qué decide que dato debe ser remplazado por uno nuevo.

  Se tiene dos tipos de cache, los de mapeo directo y los de mapeo asociativo.
  La figura \ref{img:mem_hier:cache_types} presenta el esquema de mapeo en ambos tipos. En un mapeo directo cada dato de memoria principal puede ser colocado en una única posición de memoria mientras que en un esquema asociativo puede ser colocado en varias posiciones. En la figura en cuestión se muestra el caso de asociatividad 2. Cuando el dato de memoria principal puede ser colocado en cualquier posición de la cache se tiene un esquema completamente asociativo\cite{book:Patterson2017}.

  \begin{figure}
    \centering
    \caption{Mapeo de direcciones en caches de tipo directo (izquierda) y asociativo de 2 vías (derecha). Adaptado de \href{https://commons.wikimedia.org/wiki/File:Cache,associative-fill-both.png}{Wikimedia Commons}.}
    \label{img:mem_hier:cache_types}
    \trule
    \resizebox{\textwidth}{!}{
      \includegraphics{imagenes/cache_types.tikz}
    }
    \brule
  \end{figure}

  % \begin{figure}[H]
  %   \centering
  %   \caption{Mapeo de memoria principal a distintos tipos de cache. Obtenido de \cite{book:Patterson2017}.}
  %   \label{img:mem_hier:cachetables}
  %   \trule
  %   \resizebox{\textwidth}{!}{
  %     \includegraphics{imagenes/cachetables.pdf}
  %   }
  %   \brule
  % \end{figure}

  Los datos en la cache se almacenan agrupados en bloques. Para ubicar un bloque se compara el \textit{tag} del bloque, esto forma parte de la dirección del bloque, la otra parte corresponde al \textit{index}, este indica las posiciones en cache donde se puede almacenar el dato. La dirección de memoria cuenta con bits de \textit{offset} que no son utilizados para verificar si se tiene o no el dato.

  Cuando el cache no tiene espacio disponible para un dato, este debe ser substituido. El dato que se substituye es seleccionado de acuerdo a un algoritmo. Algunos algoritmos son FIFO, LRU y aleatorio. En el primero el primer dato en entrar es el primero en salir del cache, en LRU el dato que fue accesado menos recientemente es reemplazado y en el caso de la substitución aleatoria un dato es seleccionado aleatoriamente para ser remplazado\cite{book:Patterson2017}.

  Otro aspecto a tomar en cuenta con respecto a las memorias cache es su comportamiento al solicitarse una escritura. Se puede dar el caso en que el dato se escriba de una vez en memoria principal, esto es conocido como \textit{write through}. Cuando el dato solo se escribe en memoria cache se conoce como \textit{write back}. Es hasta el momento en que se remplaza el dato cuando se escribe este en memoria principal\cite{book:Patterson2017}.


  % \section{ISAs}

  % \section{Lenguajes de Descripción de \textit{Hardware}}
