\section{Automatización de los procesos}

Si bien la herramienta vivado posee una interfaz gráfica (\textit{GUI}, de \textit{Graphic User Interface}), sus utilidades son accesibles sin la necesidad de un \textit{GUI}. Vivado ofrece una consola Tcl que permite ejecutar comandos para manejar la aplicación, esta consola está disponible en el \textit{GUI}. Se puede acceder a esta consola sin la necesidad del \textit{GUI}, abriendo la aplicación de la siguiente manera: \texttt{vivado -mode tcl}. Los comandos de Tcl pueden agruparse en \textit{scripts}, útiles en este caso para automatizar las tareas que se deben realizar repetidamente en el flujo de trabajo. Para llamar un \textit{script} se puede ejecutar el comando \texttt{source script.tcl} o bien ejecutar la aplicación de la siguiente manera, \texttt{vivado -mode batch -source script.tcl}. Xilinx ofrece una referencia para el \textit{scripting} en Tcl en \cite{vivadotcl}.

Dentro de los ejemplos de Digilent, se utilizan \textit{scripts} para automatizar algunas tareas como la creación de un proyecto. Tomando esto en cuenta se decide construir un proyecto autocontenido que pueda ejecutar las tareas de creación del proyecto, síntesis, implementación, generación del \textit{bitstream} y simulación. Se utiliza un \textit{script} base que se encarga de llamar a los demás \textit{scripts}. Las herramientas que se utilizan son bash y Tcl, también se utiliza git para mantener el registro de los cambios realizados y para poder seguir los cambios realizados en el proyecto PicoRV32.

\noindent\textbf{Estructura del Proyecto}

El proyecto se organiza de la siguiente manera:

\begin{minted}{text}
+ readme.md                instrucciones de uso
+ scripts/                 scripts en bash y en tcl
|   + create_project.sh      script para manejar scripts tcl
|   + create_project.tcl     script para crear proyecto en Vivado
|   + program_hw.tcl         script para programar un archivo .bit en la fpga
|   + run_project.tcl        script para correr síntesis, implementación y
|                                     generar bitstream
+ src/                     fuentes del proyecto
    + constraints/           archivos de localización de pines
    |   + system.xdc         un archivo de localización de pines
    + firmware/            fuentes para generar instrucciones para el
    |                              procesador
    |   + firmware.c         código c
    |   + firmware.lds       organización de la memoria
    |   + firmware.S         código ensamblador inicial
    |   + Makefile           Makefile para generar binarios
    |   + makehex.py         script para generar representación hexadecimal
    |                               del binario
    + PicoRV32/              proyecto PicoRV32 externo
    + verilog/               fuentes en verilog
        + system.v             el sistema incluye el procesador y otras partes
                                  para la implementación en la tarjeta de
                                  desarrollo
\end{minted}

\noindent\textbf{Agregando un \texttt{subtree} para el Proyecto Externo PicoRV32}

Como se mencionó previamente, se utiliza la herramienta \texttt{git} para registrar los cambios en el proyecto. También se utiliza la herramienta para manejar la dependencia que se tiene del proyecto PicoRV32. Se puede revisar el repositorio en el siguiente enlace\footnote{Requiere estar autenticado en Gitlab.}.

\url{https://gitlab.com/emilio93/inicio-ie424}

Se utiliza el procedimiento en \cite{gitsubtreeblog} para agregar la dependencia PicoRV32, para el caso concreto del proyecto se desarrolla de la siguiente manera.

\begin{minted}{bash}
cd mi-proyecto # entrar a la carpeta donde se desarrolla el proyecto
               # se asume que los archivos ya existen, la carpeta
               # src/picorv32 sin embargo, no.

# Primeros Pasos para el Repositorio
  git init    # iniciar repositorio
  git add .   # agregar archivos al repositorio, se debe asegurar que se
              # han agregado únicamente los archivos necesarios
  git status  # revisar estado del repositorio
  git commit -m "commit inicial" # agregar primer commit

# Agregando la dependencia
  # primero se agrega un repositorio remoto, se nombra PicoRV32 y se obtiene
  # el proyecto (con -f, fetch, es decir, no se aplican cambios)
  git remote add -f picorv32 https://github.com/cliffordwolf/picorv32
  # se agrega el subtree en la carpeta src/picorv32, se usa la rama master
  # del repositorio remoto picorv32, la bandera --squash indica que toda la
  # historia del repositorio remoto se comprime en un solo commit, esta bandera
  # podría ser omitida si se desea almacenar la historia del otro repositorio.
  git subtree add --prefix src/picorv32 picorv32 master --squash
\end{minted}

Con esto se ha agregado el código del repositorio externo en nuestro proyecto.

\noindent\textbf{\textit{Script} para Manejar los \textit{Scripts} Tcl}

El archivo \texttt{scripts/create\_project.sh} se utiliza para llamar los \textit{scripts} tcl habiendo asignado variables de entorno importantes como el nombre del proyecto, carpeta donde se guarda el proyecto de Vivado entre otras. El uso más sencillo es el siguiente:

\begin{minted}{bash}
  cd scripts
  ./create_project.sh -ic -ir -ip
\end{minted}

Esto crea el proyecto de Vivado (\texttt{-ic} o \texttt{-}\texttt{-is-create}), ejecuta la síntesis e implementación, genera el \textit{bitstream} (\texttt{-ir} o \texttt{-}\texttt{-is-run}) y finalmente programa el dispositivo, que debe estar conectado a la computadora y encendido (\texttt{-ip} o \texttt{-}\texttt{-is-program}). El \textit{script} no hace nada si no se le pasa alguna de las bandera mencionadas. Las banderas se pueden combinar como se desee, pero por ejemplo pasar la bandera \texttt{-ic} si el proyecto de Vivado ya existe, resulta en un error. Utilizar el \textit{script} con las banderas \texttt{-ic -ip} (suponiendo que el proyecto de Vivado no existe) también resulta en un error, puesto que la bandera \texttt{-ir} es requerida para generar el \textit{bitstream}, es decir, se requiere tener una implementación lista. Se habilitan otras banderas que pueden ser consultadas en el \textit{script}, sin embargo resulta conveniente utilizar los valores por defecto o cambiar estos según las necesidades específicas para que el llamado al \textit{script} sea más sencillo, es decir, indicar únicamente las acciones a ejecutar. La bandera para simulación se expone más adelante.

Para ejecutar el \textit{script} se debe tener la carpeta con los ejecutables de Vivado en la variable de entrono \texttt{\$PATH}, se puede comprobar de la siguiente manera:

\begin{minted}{bash}
  which vivado # debería imprimir /opt/Xilinx/Vivado/2018.2/bin/vivado
               # o algo similar
\end{minted}

Si no se imprime nada significa que la carpeta no se encuentra en la variable de entorno \texttt{\$PATH}, las instrucciones de instalación de Vivado (ver \ref{apendice:instalacion_vivado}) detallan como hacerlo de manera permanente, una solución temporal (funciona en la terminal que se utiliza) es la siguiente.

\begin{minted}{bash}
  ls /opt/Xilinx/Vivado/2018.2/bin/ # comprobar existencia de ejecutables
  ls /opt/Xilinx/Vivado/2018.2/bin/vivado # y de vivado específicamente
  PATH="/opt/Xilinx/Vivado/2018.2/bin/:$PATH" # agregar carpeta a $PATH
  which vivado # corroborar que se agregó la carpeta
  # ejecutar el script en esta carpeta
\end{minted}

\noindent\textbf{Tcl para Creación del Proyecto en Vivado}

El archivo \texttt{scripts/create\_project.tcl} crea un proyecto de Vivado. Utilizando el \textit{script} \texttt{scripts/create\_project.sh} de la manera propuesta en el apartado anterior, se encargará de compilar el código en la carpeta \texttt{firmware}. Luego crea el proyecto en una nueva carpeta \texttt{vivado}. El archivo para abrir el proyecto corresponde a \texttt{vivado/picorv32.xpr}. Se asigna la tarjeta de desarrollo Nexys 4DDR. Se agregan las fuentes para la síntesis y la simulación, así como el archivo de localización de pines, también se agrega el archivo \texttt{firmware/firmware.hex}, las instrucciones a ejecutar, por esto debe haber sido generado previamente. También se configuran las tareas de síntesis e implementación.

Cuando se realicen cambios en el proyecto, resulta adecuado actualizar este archivo para que al compartir el proyecto se pueda recrear los cambios realizados sin la necesidad de repetir los cambios. Se presenta el caso en que se quiere agregar la visualización del contador en las pantallas de siete segmentos. Se toma un módulo de los ejemplos de Digilent\footnote{El ejemplo en cuestión es \url{https://github.com/Digilent/Nexys-4-DDR-Keyboard}.} para la tarjeta Nexys 4DDR. El nuevo archivo \texttt{Seg\_7\_Display.v} se agrega a la carpeta \texttt{src/verilog/}, y debe ser agregado en el \textit{script} tcl para la creación del proyecto.

\begin{minted}{tcl}
...
# Add conventional sources
add_files $src_dir/picorv32/picorv32.v
add_files $src_dir/verilog/Seg_7_Display.v # se agrega esta línea
add_files $src_dir/verilog/system.v
add_files $src_dir/firmware/firmware.hex
...
\end{minted}

También se debe actualizar el archivo de localización de pines \texttt{src/constraints/system.xdc}, se agregan las siguientes líneas para conectar las pantallas de siete segmentos.

\begin{minted}{text}

##7 segment display

set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33 }
[get_ports { seg[0] }];
set_property -dict { PACKAGE_PIN R10   IOSTANDARD LVCMOS33 }
[get_ports { seg[1] }];
...
set_property -dict { PACKAGE_PIN L18   IOSTANDARD LVCMOS33 }
[get_ports { seg[6] }];

set_property -dict { PACKAGE_PIN H15   IOSTANDARD LVCMOS33 }
[get_ports { dp }];

set_property -dict { PACKAGE_PIN J17   IOSTANDARD LVCMOS33 }
[get_ports { an[0] }];
set_property -dict { PACKAGE_PIN J18   IOSTANDARD LVCMOS33 }
[get_ports { an[1] }];
...
set_property -dict { PACKAGE_PIN U13 IOSTANDARD LVCMOS33 }
[get_ports { an[7] }];
\end{minted}

Las señales \texttt{seg} y \texttt{an} indican el valor a desplegar y un bit para encender o apagar las pantallas respectivamente. En el módulo \texttt{system} se deben agregar como salidas, y son generadas por el módulo \texttt{seg7decimal}.

\begin{minted}{verilog}
module system (
  ...
  output [6:0] seg,
  output [7:0] an,
  output dp
);
  ...
  seg7decimal sevenSeg (
    .x(out_byte), # para evitar warnings usar .x({24'b0,out_byte}),
    .clk(clk),
    .seg(seg[6:0]),
    .an(an[7:0]),
    .dp(dp)
  );
  ...
endmodule
\end{minted}


\noindent\textbf{Tcls para Correr Tareas del Proyecto en Vivado y Programar el Dispositivo}

Los archivos \texttt{scripts/run\_project.tcl} y \texttt{scripts/run\_project.tcl} se utilizan para correr las tareas del proyecto y programar el dispositivo respectivamente. Los comandos utilizados en estos \textit{scripts} son los mismos que se ejecutarían al realizar el proceso en el GUI. Cuando se ha programado el dispositivo es necesario aplicar una señal de \textit{reset} para que se corra el programa, esto se hace cambiando la posición del interruptor V10.

\noindent\textbf{Simulando el Proyecto}

Se tiene en mente que una simulación es más lenta que la ejecución en el dispositivo. Por esto se reduce el valor del límite que se cuenta para cambiar el valor del contador del proyecto, esto permite ver en esencia el mismo comportamiento sin tener que esperar una gran cantidad de tiempo para que se ejecute toda la simulación. En el archivo \texttt{src/firmware/firmware.c} se cambia la definición de \texttt{límite}.

\begin{minted}{c}
  const unsigned int limite = 10;
\end{minted}

Se agrega un \textit{script} que automatice la simulación. Este requiere saber donde se encuentra el proyecto (\texttt{dest\_dir}) y el nombre del proyecto (\texttt{proj\_name}). Estos valores se obtienen de variables de entorno asignadas en el \textit{script} en bash.
El proyecto se abre y se lanza una simulación por 30$\mu$s. Con los cambios realizados al código se puede ver una iteración completa del contador, es decir, muestra los 256 posibles valores del contador. Finalmente se abre el \textit{GUI} para visualizar la simulación.

\begin{minted}{tcl}
# Get from env vars
set dest_dir   "$::env(DESTINY)"
set proj_name  "$::env(NAME)"

open_project $dest_dir/$proj_name.xpr

launch_simulation
restart
run 30us
start_gui
\end{minted}

Se agrega la bandera \texttt{-is}, \texttt{--is-simulate} en el \textit{script} en bash para poder llamar la simulación desde acá.

\begin{minted}{bash}
...
is_simulate=0
...
while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
  ...
    -is|--is-simulate)
      is_simulate=1
      shift # past argument
      ;;
    *)    # unknown option
      POSITIONAL+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done
...
if [ "$is_simulate" -eq "1" ]; then
  cd ${SOURCE}/firmware
  make
  cd $curwordir
  mkdir -p logs
  vivado -nojournal -log logs/simulate_project.log \
  -mode batch -source simulate_project.tcl
fi
...
\end{minted}

Con estos cambios se puede revisar la simulación ejecutando.

\begin{minted}{bash}
cd scripts
./create_project.sh -ic -is
\end{minted}
