\section{Circuitos Digitales}

  Como ya se comentó, Claude Shannon fue quien dio a conocer la posibilidad de aplicar el álgebra booleana en forma de circuitos digitales. Esto es presentado en su tesis de maestría\cite{article:Shannon1938} titulada \textit{A Symbolic Analysis of Relay and Switching Circuits}. El autor muestra cómo un circuito eléctrico utilizando interruptores puede representar una expresión booleana.

  La simbología utilizada hoy en día para los circuitos digitales ha sido desarrollada posteriormente y se especifican en el estándar ANSI/IEEE 91-1984\cite{book:Floyd2007}\cite{standard:ieee91_1984}. Si bien Shannon expuso su tesis utilizando relés, hoy en día se utilizan mayoritariamente semiconductores como interruptores, esto ha permitido la miniaturización y gran escalamiento de los circuitos digitales. Es importante recordar que un circuito digital es una representación más de las expresiones booleanas y también puede ser construido como un circuito eléctrico.

  \subsection{Compuertas Lógicas}

    Los tres operadores básicos del álgebra booleana (ver \ref{subsec:conceptos_boole}) se representan en un circuito
    digital de la manera que se muestra en el cuadro \ref{tabla:circuitos_digitales:equiv_bool_compuertas}.

    \begin{table}
      \centering
      \caption{Equivalencia de operadores booleanos y compuertas lógicas.}
      \label{tabla:circuitos_digitales:equiv_bool_compuertas}
      \trule
      \begin{subfigure}{0.45\textwidth}
        \centering
        Compuerta AND equivalente a producto booleano A$\cdot$B=C
      \end{subfigure}
      \begin{subfigure}{0.45\textwidth}
        \centering
        \begin{circuitikz}
          \draw
            node [american and port] (andgate) {}
            (andgate.in 1) node [left] {A}
            (andgate.in 2) node [left] {B}
            (andgate.out)++(0.2,0) node [right] {C}
          ;
        \end{circuitikz}
      \end{subfigure}
      \vspace*{0.25cm}
      \mrule
      \vspace*{0.25cm}
      \begin{subfigure}{0.45\textwidth}
        \centering
        Compuerta OR equivalente a suma booleana A$+$B=C
      \end{subfigure}
      \begin{subfigure}{0.45\textwidth}
        \centering
        \begin{circuitikz}
          \draw
            node [american or port] (orgate) {}
            (orgate.in 1) node [left] {A}
            (orgate.in 2) node [left] {B}
            (orgate.out)++(0.2,0) node [right] {C}
          ;
        \end{circuitikz}
      \end{subfigure}
      \vspace*{0.25cm}
      \mrule
      \vspace*{0.25cm}
      \begin{subfigure}{0.45\textwidth}
        \centering
        Compuerta NOT equivalente a negación booleana $\overline{A}$
      \end{subfigure}
      \begin{subfigure}{0.45\textwidth}
        \centering
        \begin{circuitikz}
          \draw
            node[american not port](notgate){}
            (notgate.in) node [left] {A}
            (notgate.out) node [right] {$\overline{\text{A}}$}
          ;
        \end{circuitikz}
      \end{subfigure}
      \brule
    \end{table}

    Con estas compuertas lógicas se puede construir cualquier expresión booleana. Existen otras compuertas booleanas que pueden simplificar la representación de una expresión booleana como circuito digital o bien facilitar su relación con la implementación física en alguna tecnología dada. Estas otras compuertas pueden ser consultadas en el estándar mencionado\cite{standard:ieee91_1984} o bien de manera menos formal en \cite{book:cavanagh2008} y \cite{book:Floyd2007}.

  \subsection{Multiplexores}

  Los multiplexores son conjuntos de compuertas que permiten seleccionar una señal de entrada para la salida. En la figura \ref{img:circuitos_digitales:mux4a1} se muestra un multiplexor que cuenta con 4 entradas de datos, $d_0$, $d_1$, $d_2$ y $d_3$. Para poder seleccionar una de estas entradas, se requieren 2 bits de selección ($\lceil\log_2(4)\rceil$) que corresponden a las entradas $s_0$ y $s_1$. El dato se selecciona haciendo que $s_0s_1$ corresponda al subíndice del dato en sistema binario ($00_2=0, 01_2=1, 10_2=2, 11_2=3$).

  \begin{figure}
    \centering
    \caption{Multiplexor de 4 entradas y una salida con dos entradas de selección. Obtenido de \cite{book:cavanagh2008}.}
    \label{img:circuitos_digitales:mux4a1}
    \trule
      \includegraphics{imagenes/multiplexer4to1.pdf}
    \brule
  \end{figure}

  Dada la utilidad del circuito en la figura \ref{img:circuitos_digitales:mux4a1} se utiliza el símbolo en la figura \ref{img:circuitos_digitales:mux4a1diag}. Se puede tener un distinto número de entradas $N$ que requerirán una cantidad de $\lceil\log_2(N)\rceil$ bits de selección. Las entradas también pueden ser buses de datos.

  \begin{figure}
    \centering
    \caption{Diagrama para multiplexor de 4 entradas y una salida con dos entradas de selección.}
    \label{img:circuitos_digitales:mux4a1diag}
    \trule
      \begin{circuitikz}
        \draw(0,0) node (inicio) {}
        (inicio.center) -- ++(0,5) -- ++(2,-.5) -- ++(0,-4) -- ++(-2,-.5)
        (inicio.center)
        ++(0,1) node [right] {11$_2$}
        -- ++(-1,0) node [left] {$d_3$}

        (inicio.center)
        ++(0,2) node [right] {10$_2$}
        -- ++(-1,0) node [left] {$d_2$}

        (inicio.center)
        ++(0,3) node [right] {01$_2$}
        -- ++(-1,0) node [left] {$d_1$}

        (inicio.center)
        ++(0,4) node [right] {00$_2$}
        -- ++(-1,0) node [left] {$d_0$}
        (inicio.center) ++(-1,-1.25) node (selec) {} node [left] {[$s_0$,$s_1$]}
        (selec.center) -- ++(2,0) -- ++(0,1.5)
        (selec.center) ++(.9,-.2) -- ++(.2,.4) ++(-.1,0) node [above] {2}

        (inicio.center)
        ++(2,2.5) -- ++(1,0) node [right] {z$_1$}
        ;
      \end{circuitikz}
    \brule
  \end{figure}

  \subsection{Circuitos Combinacionales y Secuenciales}

    Hasta el momento se han revisado expresiones booleanas en las cuales se tiene una cierta cantidad de entradas y una salida. Los circuitos también pueden tener varias salidas, se representaría como un sistema de ecuaciones. Estos son conocidos como circuitos combinacionales. Tienen la característica que para un conjunto de entradas conocidas, se conoce cuál será la salida en todo momento.

    En la figura \ref{img:circuitos_digitales:latchd} se muestra un circuito en el cual no es suficiente conocer sus entradas para conocer su salida en todo momento. La compuerta utilizada se conoce como NAND, la cuál se obtiene lógicamente realizando el producto de sus entradas y negando el resultado obtenido. En los casos en que la señal \textit{enable} (E) se encuentra en 1, la salida Q corresponde a la entrada \textit{data} (D), mientras que la salida $\overline{Q}$ corresponde a la entrada \textit{data} invertida. Cuando la señal de entrada \textit{enable} se encuentra en 0, la salida del circuito mantiene su salida previa. Cuando no se conoce su salida previa es incierto el valor de sus salidas, lo que sucede cuando se enciende el circuito con la entrada \textit{enable} en 0. Este tipo de circuitos en que se debe conocer el estado anterior del circuito para conocer cuáles serán sus salidas ante ciertas entradas, se conocen como circuitos secuenciales.

    \begin{figure}
      \centering
      \caption{\textit{Latch} tipo D.}
      \label{img:circuitos_digitales:latchd}
      \trule
      \begin{subfigure}{0.39\textwidth}
        \centering
        \begin{tabular}{cccc}
          \toprule
          \multicolumn{2}{c}{Entradas} & \multicolumn{2}{c}{Salidas}
          \\
          data & enable & Q & $\overline{Q}$
          \\\toprule
          0 & 1 & 0 & 1
          \\\midrule
          1 & 1 & 1 & 0
          \\\midrule
          X & 0 & $Q_\text{Previo}$ & $\overline{\text{Q}_\text{Previo}}$
          \\\bottomrule
        \end{tabular}
      \end{subfigure}
      \begin{subfigure}{0.6\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{imagenes/D-Type_Transparent_Latch.pdf}
      \end{subfigure}
      \brule
    \end{figure}

    Ahora se utilizan dos de los circuitos en la figura \ref{img:circuitos_digitales:latchd} unidos en la manera que lo muestra la figura \ref{img:circuitos_digitales:ffd}. Ahora el valor de salida se actualiza únicamente cuando la señal \textit{clock} (C) pasa de 0 a 1, es decir, en el flanco positivo de la señal. La señal \textit{clock} suele utilizarse en grandes cantidades de \textit{flip-flops} para lograr una sincronización en todo un circuito digital de la manera deseada.

    \begin{figure}
      \centering
      \caption{\textit{Flip-Flop} tipo D.}
      \label{img:circuitos_digitales:ffd}
      \trule
      \begin{subfigure}{0.39\textwidth}
        \centering
        \begin{tabular}{cp{1.3cm}cc}
            \toprule
            \multicolumn{2}{c}{Entradas} & \multicolumn{2}{c}{Salidas}
            \\
            data & clock & Q & $\overline{Q}$
            \\\toprule
            0 & flanco positivo & 0 & 1
            \\\midrule
            1 & flanco positivo & 1 & 0
            \\\midrule
            X & 0 & $Q_\text{Previo}$ & $\overline{\text{Q}_\text{Previo}}$
            \\\bottomrule
        \end{tabular}
      \end{subfigure}
      \begin{subfigure}{0.6\textwidth}
        \centering
        \includegraphics[width=\textwidth]
        {imagenes/D-Type_Flip-flop_Diagram.pdf}
      \end{subfigure}
      \brule
    \end{figure}

    Dada la ubicuidad de los \textit{flip-flops} en los circuitos digitales, se utiliza más comúnmente un diagrama simplificado para representarlo dentro de un circuito digital. Dicho diagrama se muestra en la figura \ref{img:circuitos_digitales:ffd_caja}.

    \begin{figure}
      \centering
      \caption{Diagrama utilizado para un \textit{flip-flop} tipo D.}
      \label{img:circuitos_digitales:ffd_caja}
      \trule
      \begin{circuitikz}
        \draw (0,0) -- ++(0,3) -- ++(2,0) -- ++(0,-3) -- ++(-2,0);
        \draw (0,0) ++(0,.9) -- ++(.2,.1) -- ++(-.2,.1);
        \draw (0,1) -- ++(-1,0) node [left] {\texttt{C}};
        \draw (0,2) -- ++(-1,0) node [left] {\texttt{D}};
        \draw (2,1) -- ++(1,0) node [right] {$\overline{\texttt{Q}}$};
        \draw (2,2) -- ++(1,0) node [right] {\texttt{Q}};
        \draw (1,3) node [below] {\texttt{FFD}};
      \end{circuitikz}
      \brule
    \end{figure}

    Si bien se ha presentado el \textit{flip-flop} tipo D, existen otros tipos de \textit{flip-flops} como el SR y el JK\cite{book:Floyd2007}.

  \subsection{Sistemas Digitales}

    Los sistemas digitales sirven para presentar circuitos digitales complejos de una manera más simple. Una porción del circuito es representada en un solo bloque. El bloque se encarga de realizar una serie de tareas determinadas. Las tareas las realiza en función de señales de entrada al bloque. El bloque obtiene el resultado de las tareas como señales de salida. Los bloques son interconectados posteriormente para crear el circuito digital deseado.

    Este enfoque permite que se puedan probar partes separadas del circuito. A su vez simplificando encontrar donde surge un error. También permite reutilizar un bloque en múltiples circuitos.

  \subsection{Resumen}

    Se ha presentado una representación útil de las expresiones booleanas conocida como circuito digital y se han mostrado las representaciones para los operadores booleanos presentados en \ref{subsec:conceptos_boole}. A partir de las compuertas lógicas se han mostrado circuitos lógicos de importancia como lo son los multiplexores, \textit{latches} y \textit{flip-flops}. Finalmente se observó que los circuitos digitales se pueden conectar unos con otros para crear sistemas digitales de alta complejidad. Uno de estos sistemas es la computadora digital, el cuál será presentado en la siguiente sección.
