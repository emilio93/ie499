\chapter{Nota Teórica}
\label{C:antecedentes}

\section{Álgebra Booleana}

  En \cite{book:boole1854} George Boole complementa su contenido expuesto en \cite{book:boole1847}. Este conjunto formula la base teórica con la que se ha desarrollado el mundo digital como lo conocemos hoy en día. si bien estos textos fueron publicados en 1854 y 1847 respectivamente, no fue hasta 1938 cuando Claude Shannon aplicó la teoría expuesta por Boole en la creación de circuitos digitales\cite{book:Floyd2007}.

  Boole argumenta a partir de una serie de principios y postulados, leyes y proposiciones que se pueden afirmar sobre el pensamiento razonado realizado por la mente humana. Lo importante no es la idea sobre la cual realiza su estudio, puesto que esto ya había sido tratado en tiempos anteriores, principalmente por Aristóteles
  \cite{book:boole1854}, es más bien la formalidad matemática con la que el autor logra presentar sus ideas lo que hace de estas obras fundamentales en el desarrollo de los sistemas digitales.

  La manera en la que Boole expone su teoría es clara y concisa, sin embargo puede resultar difícil de interpretar dada la formalidad y generalidad con que se desarrolla, otros autores exponen la teoría en una manera accesible y enfocada a los sistemas digitales (\cite{book:Floyd2007}, \cite{book:cavanagh2008}).

  \subsection{Conceptos Básicos del Álgebra de Boole}
    \label{subsec:conceptos_boole}

    El conjunto utilizado en el álgebra de boole se compone de dos símbolos, generalmente 0 y 1, pero lo importante es reconocer que estos símbolos son distintos uno de otro. Formalmente se tiene el conjunto $\mathbb{B} = \{0,1\}$. En una expresión booleana se tiene cierta cantidad de variables a las cuales se les aplica ciertos operadores, estas variables equivalen a uno de los elementos de $\mathbb{B}$. Todas las operaciones realizadas tienen como resultado uno de los elementos de $\mathbb{B}$. Existen tres operaciones básicas: suma ($A+B$), producto ($A\cdot B$) y negación ($A'$, $\overline{A}$). Este último también es llamado complemento y otros operadores pueden ser definidos a partir de estos\cite{book:cavanagh2008}.

    El álgebra de boole hace uso de las leyes conmutativa, asociativa y distributiva que se conocen de la teoría de conjuntos\cite{book:Floyd2007}\cite{book:kolman1997}.

    \begin{align}
      \tag{Ley Conmutativa para la Suma}
      A + B &= B + A
      \label{eq:bool:ley_conmutativa_suma}
      \\
      \tag{Ley Conmutativa para el Producto}
      A\cdot B &= B\cdot A
      \label{eq:bool:ley_conmutativa_producto}
      \\
      \tag{Ley Asociativa para la Suma}
      A+(B+C) &= (A+B)+C
      \label{eq:bool:ley_asociativa_suma}
      \\
      \tag{Ley Asociativa para el Producto}
      A\cdot(B\cdot C) &= (A\cdot B)\cdot C
      \label{eq:bool:ley_asociativa_producto}
      \\
      \tag{Ley Distributiva}
      A\cdot(B+C) &= A\cdot B + A\cdot C
      \label{eq:bool:ley_distributiva}
    \end{align}

    Los resultados para la operación de suma, producto y negación se muestran a continuación.
    \begin{align*}
      \tag{Resultados de Suma Booleana}
      0+0 &= 0 & 0+1 &= 1 & 1+1 &= 1
      \label{eq:bool:resultados_suma}
    \end{align*}
    \begin{align*}
      \tag{Resultados de Producto Booleano}
      0\cdot 0 &= 0 & 0\cdot 1 &= 0 & 1\cdot 1 &= 1
      \label{eq:bool:resultados_producto}
    \end{align*}
    \begin{align*}
      \tag{Resultados de Negación Booleano}
      0' = \overline {0} &= 1 & 1' = \overline {1} &= 0
      \label{eq:bool:resultados_negacion}
    \end{align*}

  \subsection{Reglas Derivadas y Teoremas de DeMorgan}

    A continuación se presentan reglas que pueden ser comprobadas al aplicar las leyes y resultados presentados en \ref{subsec:conceptos_boole}.

    \begin{equation*}
      \tag{Regla 1}
      A+0=A
      \label{eq:bool:regla1}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 2}
      A+1=1
      \label{eq:bool:regla2}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 3}
      A\cdot 0=0
      \label{eq:bool:regla3}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 4}
      A\cdot 1=A
      \label{eq:bool:regla4}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 5}
      A+A=A
      \label{eq:bool:regla5}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 6}
      A+\overline {A}=1
      \label{eq:bool:regla6}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 7}
      A\cdot A=A
      \label{eq:bool:regla7}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 8}
      A\cdot \overline {A}=0
      \label{eq:bool:regla8}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 9}
      \overline {\overline {A}} = A
      \label{eq:bool:regla9}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 10}
      A+A\cdot B=A
      \label{eq:bool:regla10}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 11}
      A+\overline {A}\cdot B=A+B
      \label{eq:bool:regla11}
    \end{equation*}
    \begin{equation*}
      \tag{Regla 12}
      (A+B)\cdot(A+C)=A+B\cdot C
      \label{eq:bool:regla12}
    \end{equation*}

    Los teoremas de DeMorgan son de importancia, proponen lo siguiente.

    \begin{equation*}
      \tag{Primer Teorema de DeMorgan}
      \overline {A\cdot B} = \overline {A} + \overline {B}
      \label{eq:bool:morgan1}
    \end{equation*}
    \begin{equation*}
      \tag{Segundo Teorema de DeMorgan}
      \overline {A+ B} = \overline {A} \cdot \overline {B}
      \label{eq:bool:morgan2}
    \end{equation*}

  \subsection{Simplificación de Expresiones Booleanas}

    Aplicando las leyes, reglas y teoremas presentados en secciones anteriores a expresiones booleanas se puede lograr reducir la complejidad de esta sin afectar el resultado de la expresión, dando como resultado dos expresiones booleanas equivalentes.

    Tabulando los posibles valores de las variables contra el resultado de una expresión para estos valores se construye una tabla de verdad. Esta contiene todos los posibles resultados para una expresión booleana. Por ejemplo para $\overline{A\cdot B}$.

    \begin{center}
      \begin{tabular}{ccp{0.5cm}c}
      \toprule
      $A$ & $B$ & & $\overline{A\cdot B}$
      \\\toprule
      0 & 0 && 1
      \\
      0 & 1 && 1
      \\
      1 & 0 && 1
      \\
      1 & 1 && 0
      \\\bottomrule
      \end{tabular}
    \end{center}

    A partir de una tabla de verdad se puede obtener una expresión equivalente. Al sumar los resultados que son 1, se puede aplicar la \ref{eq:bool:regla2} y saber que el resultado de esta suma es 1. Para obtener una expresión a esta suma, se puede aplicar negación y multiplicación de los valores que toman las variables en la tabla de verdad. Para el primer resultado de la tabla de verdad que se acaba de mostrar, $\overline{A}\cdot\overline{B}=1$, para los siguientes dos resultados se tiene respectivamente $A\cdot\overline{B}=\overline{A}\cdot B=1$. Ahora se sabe que sumar estas tres expresiones se va a tener un resultado de 1. Se puede afirmar que la expresión $\overline{A}\cdot\overline{B} + A\cdot\overline{B} + \overline{A}\cdot B$ es equivalente a la expresión utilizada en la tabla, $\overline{A\cdot B}$. Se puede comprobar aplicando las leyes, reglas y teoremas presentados.
    \begin{align*}
      &\overline{A}\cdot\overline{B} + A\cdot\overline{B} + \overline{A}\cdot B
      \\
      & \text{Se aplica {\ref{eq:bool:ley_distributiva}}} \Rightarrow
      \\
      &\overline{A}\cdot(\overline{B}+B) + A\cdot\overline{B}
      \\
      & \text{Se aplica \ref{eq:bool:regla6}} \Rightarrow
      \\
      &\overline{A}\cdot 1 + A\cdot\overline{B}
      \\
      & \text{Se aplica \ref{eq:bool:regla4}} \Rightarrow
      \\
      &\overline{A} + A\cdot\overline{B}
      \\
      & \text{Se aplica \ref{eq:bool:regla11}} \Rightarrow
      \\
      &\overline{A} + \overline{B}
      \\
      & \text{Se aplica \ref{eq:bool:morgan1}} \Rightarrow
      \\
      &\overline{A\cdot B}
    \end{align*}
    La expresión que se ha obtenido a partir de la tabla de verdad se conoce como una suma de productos. Se puede obtener similarmente un producto de sumas. Para la misma tabla se obtendría $(\overline{A} + \overline{B})$.

    Otra herramienta útil para la simplificación de expresiones booleanas son los mapas de Karnaugh, estos muestran todos los posibles resultados de una expresión booleana haciendo evidente la relación que existe entre las variables y la expresión. La figura \ref{notateori:karnaugh} se presenta una expresión booleana representada como un mapa de Karnaugh. Se tienen las variables W, X, Y, Z, la línea adjunta a las letras representa el área dentro del mapa donde el valor de la variable es 1. Por ejemplo, la mitad derecha del mapa corresponde a un valor de Y=1, mientras que la mitad superior del mapa corresponde a un valor de W=0. Así los cuatro cuadros en la esquina superior derecha corresponden a $\overline{\text{W}}\text{Y}$.

    De la misma manera, se puede obtener que la expresión para los valores en el mapa corresponde a $\text{WXY}$. Puesto que para ubicar los valores de 1 todos estos términos deben tener un valor de 1, el caso de $\text{Z}$ no hace diferencia si se trata de un 1 o un 0. Pasar una tabla de verdad a su representación en un mapa de Karnaugh puede representar una menor complejidad a la hora de obtener la expresión booleana en su representación matemática\footnote{Existen técnicas que permiten sistematizar la obtención de la expresión matemática, estas se pueden consultar en las referencias \cite{book:Floyd2007} y \cite{book:cavanagh2008} .}.

    \begin{figure}
      \centering
      \caption{Ejemplo de un mapa de Karnaugh.}
      \label{notateori:karnaugh}
      \trule
      \begin{subfigure}{.4\textwidth}
        \centering
        \begin{Karnaugh}{W}{X}{Y}{Z}
          \minterms{14, 15}
          \implicant{15}{14}{blue}
        \end{Karnaugh}
    \end{subfigure}
    \brule
    \end{figure}

  \subsection{Resumen}

    Se ha dado una reseña histórica del desarrollo del álgebra booleana en torno a
    los circuitos digitales y se han revisado los conceptos más relevantes de esta teoría enfocados a los sistemas digitales. Se ha visto que a partir de un conjunto de leyes básicas y postulados se logra derivar toda una serie de reglas y teoremas que se cumplen. La abstracción matemática presentada es utilizada para modelar circuitos digitales puesto que con dos símbolos únicamente, se puede obtener salidas conocidas a partir de entradas que pueden variar, en esto se fundamentan los sistemas digitales. La siguiente sección presenta más en detalle las bases de los circuitos digitales y su relación con el álgebra booleana.

  \newpage
