\section{Laboratorio 4: Periféricos en la Tarjeta de Desarrollo}

\subsection{Objetivo General}

Utilizar los periféricos de la tarjeta de desarrollo.

\subsection{Objetivos Específicos}

\begin{itemize}
  \item Investigar cómo utilizar periféricos en la tarjeta de desarrollo.
  \item Utilizar un procesador para manejar los periféricos de la tarjeta de desarrollo.
\end{itemize}

\subsection{Propuesta del Problema}

La tarjeta de desarrollo cuenta con un puerto VGA, para utilizarlo debe manejarse 14 señales, 12 de estas señales corresponden al color desplegado en cierto pixel, 4 para cada uno de los colores principales, rojo, verde y azul, las otras dos señales se conocen como HSYNC (horizontal sync) y VSYNC (vertical sync), se utilizan para estar refrescando la imagen constantemente. El cuadro \ref{lab4:tabvga} muestra las señales.

\begin{table}[h]
  \centering
  \caption{Señales utilizadas en puerto VGA.}
  \label{lab4:tabvga}
  \trule
  \begin{tabular}{ccc}
  \textbf{Señal} & \textbf{Descripción} & \textbf{Pin} \\\toprule
  \texttt{vga\_red\_o[0]} & Rojo (MSB) & A3\\
  \texttt{vga\_red\_o[1]} & Rojo & B4\\
  \texttt{vga\_red\_o[2]} & Rojo & C5\\
  \texttt{vga\_red\_o[3]} & Rojo (LSB) & A4 \\\midrule
  \texttt{vga\_green\_o[0]} & Verde (MSB) & C6\\
  \texttt{vga\_green\_o[1]} & Verde & A5\\
  \texttt{vga\_green\_o[2]} & Verde & B6\\
  \texttt{vga\_green\_o[3]} & Verde (LSB) & A6 \\\midrule
  \texttt{vga\_blue\_o[0]} & Azul (MSB) & B7\\
  \texttt{vga\_blue\_o[1]} & Azul & C7\\
  \texttt{vga\_blue\_o[2]} & Azul & D7\\
  \texttt{vga\_blue\_o[3]} & Azul (LSB) & D8 \\\midrule
  \texttt{HSYNC} & Sincronización horizontal & B11 \\
  \texttt{VSYNC} & Sincronización vertical & B12
  \end{tabular}
  \brule
\end{table}

Inicialmente se busca corroborar el funcionamiento del protocolo VGA en resolución 640$\times$480 refrescándose 60 veces por segundo, luego se quiere leer imágenes o patrones desde memoria.

La tarjeta de desarrollo también cuenta con un puerto USB que puede ser utilizado con múltiples dispositivos como teclados y \textit{mouses}. Se propone utilizar un teclado con un módulo que implementa el protocolo PS2, se puede obtener en los ejemplos de Digilent para la tarjeta de desarrollo.

Se propone utilizar la interfaz para interrupciones del procesador para utilizar el teclado en un programa.

\subsection{Ejercicio 1}

Investigue sobre el protocolo VGA para la resolución y frecuencia propuestos. De acuerdo a lo investigado diseñe un módulo que pueda mostrar un color en la toda la pantalla, itere sobre múltiples colores que cambien cada cierto tiempo. Utilice una señal de reloj de 25MHz para este módulo.

De acuerdo a la cantidad de señales utilizadas para el color, ¿cuántos colores se pueden mostrar? ¿Cómo se definen los colores? Dé ejemplos de secuencias de bits y el color correspondiente, indique al menos 5.

Ahora agregue contadores para identificar la posición del pixel que está siendo pintado. A partir de estos contadores dibuje una imagen en la pantalla, esta puede ser por ejemplo un tablero o una bandera. Explique brevemente como se genera la imagen.


\subsection{Ejercicio 2}

Ahora se va a utilizar una memoria para leer los colores. Se puede utilizar una memoria como la siguiente.

\begin{minted}{verilog}
module RAM_SINGLE_READ_PORT # (
  parameter DATA_WIDTH= 16,
  parameter ADDR_WIDTH=8,
  parameter MEM_SIZE=8
) (
  input wire Clock,
  input wire iWriteEnable,
  input wire[ADDR_WIDTH-1:0] iReadAddress,
  input wire[ADDR_WIDTH-1:0] iWriteAddress,
  input wire[DATA_WIDTH-1:0]
  output reg [DATA_WIDTH-1:0]
  iDataIn,
  oDataOut
);
  reg [DATA_WIDTH-1:0] Ram [MEM_SIZE:0];
  always @(posedge Clock) begin
    if (iWriteEnable)
      Ram[iWriteAddress] <= iDataIn;
    oDataOut <= Ram[iReadAddress];
  end
endmodule
\end{minted}

Esta memoria sería instanciada de la siguiente manera para funcionar como una memoria de video.

\begin{minted}{verilog}
RAM_SINGLE_READ_PORT # (
  12,
  24,
  640*480
) VideoMemory (
  .Clock(Clock),
  .iWriteEnable(rVGAWriteEnable),
  .iReadAddress(24'b0),
  .iWriteAddress(mem_addr),
  .iDataIn(iColor)
  .oDataOut(oColor)
);
\end{minted}

Realice los cambios necesarios en el procesador para poder escribir datos en la memoria de video, conecte la memoria de video con el módulo realizado en el ejercicio 1 para mostrar los datos de la VGA en la pantalla.

En caso que los recursos de la \textit{FPGA} no sean suficientes para tener la memoria de video, busque algún método para reducir el tamaño de la memoria. Por ejemplo se podría tener una imagen más pequeña de manera que se muestre un marco negro alrededor de la imagen mostrada. También podría usarse una memoria para la mitad de la resolución (320$\times$240) y agrandarla de acuerdo a la posición en la pantalla que se pinta. Otra manera de reducir el tamaño de la memoria es utilizar menos bits por color, en vez de 12 señales para el color se podría utilizar solo 6 señales, siendo así 2 bits para rojo, verde y azul. Se realizaría la asignación del dato \texttt{01} a \texttt{0011}, y de \texttt{10} a \texttt{1100} por ejemplo. Puede pensar en otras maneras de reducir la memoria requerida.

Escriba un código en C que dibuje un tablero en la pantalla, esto se hace escribiendo los datos en la memoria de video. Ahora escriba el código para pintar el cuadro del tablero en la esquina superior izquierda de otro color, después de cierto tiempo se debe pintar el cuadro contiguo a la derecha del mismo color, y el cuadro anterior debe pintarse del color que corresponde en el tablero, esto da la apariencia de movimiento. El código debe realizar esto hasta llegar al cuadro superior derecho. Cuando esto suceda, el cuadro deberá pasar a la fila inferior y realizar el mismo proceso en esa línea. Cuando llega a la línea más inferior del tablero y al cuadro más a la derecha en esta fila, se reinicia el proceso. Los dos primeros pasos se muestran en la figura \ref{lab4:pantalla}. Puede utilizar la cantidad de filas y columnas que desee.

\begin{figure}[H]
  \caption{Ejemplo de como se verían las dos primeras iteraciones en la pantalla.}
  \label{lab4:pantalla}
  \trule
  \centering
  \begin{tikzpicture}
    \filldraw[fill=black] (0,0) rectangle ++(1,1);
    \filldraw[fill=white] (1,0) rectangle ++(1,1);
    \filldraw[fill=black] (2,0) rectangle ++(1,1);
    \filldraw[fill=white] (3,0) rectangle ++(1,1);

    \filldraw[fill=white] (0,1) rectangle ++(1,1);
    \filldraw[fill=black] (1,1) rectangle ++(1,1);
    \filldraw[fill=white] (2,1) rectangle ++(1,1);
    \filldraw[fill=black] (3,1) rectangle ++(1,1);

    \filldraw[fill=black] (0,2) rectangle ++(1,1);
    \filldraw[fill=white] (1,2) rectangle ++(1,1);
    \filldraw[fill=black] (2,2) rectangle ++(1,1);
    \filldraw[fill=white] (3,2) rectangle ++(1,1);

    \filldraw[fill=cyan, draw=cyan] (0,3) rectangle ++(1,1);
    \filldraw[fill=black] (1,3) rectangle ++(1,1);
    \filldraw[fill=white] (2,3) rectangle ++(1,1);
    \filldraw[fill=black] (3,3) rectangle ++(1,1);

    \draw (4.2,1.5)
    -- ++(1,0) -- ++(0,-.3) -- ++(,.8) -- ++(-1,.8) -- ++(0,-.3) -- ++(-1,0)
    ;
  \end{tikzpicture}
  \begin{tikzpicture}
    \filldraw[fill=black] (0,0) rectangle ++(1,1);
    \filldraw[fill=white] (1,0) rectangle ++(1,1);
    \filldraw[fill=black] (2,0) rectangle ++(1,1);
    \filldraw[fill=white] (3,0) rectangle ++(1,1);

    \filldraw[fill=white] (0,1) rectangle ++(1,1);
    \filldraw[fill=black] (1,1) rectangle ++(1,1);
    \filldraw[fill=white] (2,1) rectangle ++(1,1);
    \filldraw[fill=black] (3,1) rectangle ++(1,1);

    \filldraw[fill=black] (0,2) rectangle ++(1,1);
    \filldraw[fill=white] (1,2) rectangle ++(1,1);
    \filldraw[fill=black] (2,2) rectangle ++(1,1);
    \filldraw[fill=white] (3,2) rectangle ++(1,1);

    \filldraw[fill=white ] (0,3) rectangle ++(1,1);
    \filldraw[fill=white] (2,3) rectangle ++(1,1);
    \filldraw[fill=black] (3,3) rectangle ++(1,1);
    \filldraw[fill=cyan, draw=cyan] (1,3) rectangle ++(1,1);
  \end{tikzpicture}
  \brule
\end{figure}

\subsection{Ejercicio 3}

Investigue acerca del protocolo utilizado para leer los datos del teclado, comente sus característica y explique brevemente el protocolo. Utilice la implementación en \url{https://github.com/Digilent/Nexys-4-DDR-Keyboard}. Revise cómo es implementado el protocolo, explique la funcionalidad del módulo \texttt{debouncer}, realice una prueba sencilla en que demuestre que el módulo funciona adecuadamente.

Investigue sobre la interfaz para interrupciones en el procesador y utilícela para poder captar las teclas presionadas en el procesador. Luego busque la manera de utilizar la tecla presionada en un código en C. Actualice el código del tablero realizado en el ejercicio 2 para que el cuadro pintado cambie su ubicación cuando se presiona una flecha, el movimiento del cuadro debe ser en la dirección que la flecha indique.

\subsection{Ejercicio 4}

Investigue sobre otros periféricos de la tarjeta de desarrollo. Cree una aplicación sencilla con este periférico. Podría generar sonido y reproducirlo con la salida de audio, crear un nivel con el acelerómetro integrado entre otras aplicaciones, también puede crear circuitos externos que funcionen a partir de señales generadas en la \textit{FPGA}.

\subsection{Ejercicio 5}

Este ejercicio es opcional.

La tarjeta de desarrollo cuenta con una memoria DDR2 de 128MiB. Investigue sobre como utilizar esta memoria en su diseño y utilícela como memoria de video.