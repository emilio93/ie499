\section{Laboratorio 3: Circuito Secuenciales}

\subsection{Objetivo General}

Utilizar el \textit{FPGA} para el desarrollo de circuitos secuenciales.

\subsection{Objetivos Específicos}

\begin{itemize}
  \item Desarrollar habilidades en el ambiente de desarrollo integrado Vivado de Xilinx.
  \item Obtener información útil de los reportes generados en las distintas etapas del flujo en Vivado.
  \item Aplicar conceptos de caches aprendidos en otros cursos y medir datos de desempeño para las implementaciones realizadas.
\end{itemize}

\subsection{Propuesta del Problema}

Se quiere implementar una jerarquía de memoria para el procesador. Esta jerarquía debe contar con memoria principal (implementación propia o memoria RAM en la tarjeta de desarrollo) y un cache. Hasta el momento se ha utilizado una memoria para las instrucciones de la siguiente manera:

\begin{minted}{verilog}
  reg [31:0] memory [0:MEM_SIZE-1];
  initial \$readmemh("firmware.hex", memory);
\end{minted}

Para leer y escribir datos en la memoria se utiliza la siguiente lógica:

\begin{minted}{verilog}
always @(posedge clk) begin
  mem_ready <= 1;
  out_byte_en <= 0;
  mem_rdata <= memory[mem_la_addr >> 2];
  if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
    if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
    if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
    if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
    if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
  end else if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
    out_byte_en <= 1;
    out_byte <= mem_la_wdata;
  end
end
\end{minted}

Esta memoria no tiene retraso, y en cada ciclo lee un dato y escribe uno si es solicitado, sin embargo es una memoria pequeña que podría no ser suficiente en muchos proyectos.

También se cuenta con la siguiente implementación, la cual se habilita mediante un parámetro.

\begin{minted}{verilog}
always @(posedge clk) begin
  m_read_en <= 0;
  mem_ready <= mem_valid && !mem_ready && m_read_en;

  m_read_data <= memory[mem_addr >> 2];
  mem_rdata <= m_read_data;

  out_byte_en <= 0;

  (* parallel_case *)
  case (1)
    mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
      m_read_en <= 1;
    end
    mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
      if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
      if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
      if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
      if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
      mem_ready <= 1;
    end
    mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
      out_byte_en <= 1;
      out_byte <= mem_wdata;
      mem_ready <= 1;
    end
  endcase
end
\end{minted}

La implementación del procesador utiliza la siguiente interfaz con la memoria.

\begin{minted}{verilog}
output        mem_valid # comienza transferencia de datos
output        mem_instr # indica que se trata de una instrucción
input         mem_ready # baja la señal mem_valid

output [31:0] mem_addr  # dirección de memoria
output [31:0] mem_wdata # dato de salida
output [ 3:0] mem_wstrb # indica modo de lectura/escritura
input  [31:0] mem_rdata # dato de entrada
\end{minted}

Con esta interfaz es posible implementar un cache que se adapte al procesador. Se puede tomar como base la implementación existente para la memoria principal o bien crear una implementación propia. Se puede utilizar también la memoria integrada en la tarjeta de desarrollo. En la figura \ref{lab3:esquema} se muestra un esquema general de lo que debe diseñar.

\begin{figure}[H]
  \caption{Esquema de conexión entre el procesador, el cache y la memoria principal.}
  \label{lab3:esquema}
  \trule
  \centering
  \includegraphics{imagenes/cache_interface.tikz}
  \brule
\end{figure}

\subsection{Ejercicio 1}

Revise la interfaz de memoria más detalladamente para entender cómo funciona esta e implemente una memoria cache que comunique una memoria principal de mayor tamaño. Parametrice el tamaño del cache para poder realizar pruebas con pocos datos pero extender el tamaño cuando sea requerido. El algoritmo de reemplazo puede ser FIFO o LRU.

El siguiente pseudocódigo indica como podría funcionar una cache.

\begin{minted}{text}
Obtener Dato en la dirección mem_addr:
    ¿Tengo el dato? (Revisar de tablas)
    Sí:
        Actualizar tablas para algoritmo utilizado si se requiere
        Devolver dato en mem_addr
    No:
        ¿Tengo espacio para almacenar dato?
        Sí:
            Obtener dato de memoria principal
            Almacenar dato
        No:
            Remplazar dato según algoritmo de substitución
            Actualizar tablas para algoritmo utilizado si se requiere
        Devolver dato
\end{minted}

Inicialmente no se tendrá ningún dato. Esto se puede implementar con un bit que indique si el dato es válido, al resetear el módulo se pueden asignar todos los bits de válido a 0.

En la estructura FIFO se tendría una tabla que se actualiza cada vez que entra un dato. Cuando la tabla está llena, el último dato es reemplazado por el nuevo dato ingresado.

En la estructura LRU se actualiza una tabla cada vez que un dato es solicitado, este dato obtiene la posición que indique que fue el dato más recientemente utilizado. Cuando la tabla está llena, el dato menos recientemente utilizado es reemplazado por el nuevo dato que adquiere la posición del dato más recientemente utilizado.

Indique el tamaño de las memorias (cache y principal), tipo de asociatividad utilizado, ¿Por qué escogió este tipo?

Escriba un código con el cual pueda corroborar el correcto funcionamiento del cache implementado.

Obtenga los resultados de los reportes de utilización y temporización para su implementación. Comente los cambios observados al tener una memoria cache.

\subsection{Ejercicio 2}

La implementación del procesador provee una cuenta de ciclos transcurridos así como una cuenta de instrucciones ejecutadas. Para el código realizado obtenga el dato de ciclos por instrucción(CPI) promedio. Escriba otro código que mejore el dato CPI obtenido  y otro código que lo empeore. En caso de no poder mejorar o empeorar su código comente por qué no fue posible. ¿Para qué aplicaciones es útil el algoritmo de reemplazo utilizado?

Agregue contadores al cache implementado en el ejercicio 1 para obtener información de los \textit{hits} y \textit{misses} obtenidos, obtenga el \textit{missrate}. Obtener tiempos de acceso promedio a memoria.

\subsection{Ejercicio 3}

Investigue sobre algoritmos de reemplazo más complejos y realice una implementación de este, obtenga las mismas mediciones que realizó en el ejercicio 2. Indique en qué aplicaciones es adecuado el algoritmo seleccionado.