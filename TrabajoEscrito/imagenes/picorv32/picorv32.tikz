\begin{tikzpicture}[font=\ttfamily]
  \def \textVStep {.8cm}
  \def \inputLineLength {2cm}
  \def \busVSpace {.4cm}
  \def \busHSpace {.2cm}

  \def \procHeight {23*\textVStep}
  \def \procWidth {5cm}

  \newcommand{\busLine}[2]{
    \draw (#1.center)
    (#1.center) ++(\busHSpace/2, -\busVSpace/2) node (p1) {}
    (#1.center) ++(-\busHSpace/2, \busVSpace/2) node (p2) {}
    (p1.center) -- (p2.center)
    (#1.center) ++(\busHSpace/4,\busVSpace/2) node [right] {#2}
    ;
  }

  \draw (0,0) node (inicio) {};

  \draw (inicio.center)
  rectangle ++(\procWidth, \procHeight)
  (inicio.center) ++(\procWidth, \procHeight) node (proc_top_right) {}
  (inicio.center) ++(\procWidth, 0) node (proc_bot_right) {}
  (inicio.center) ++(0, \procHeight) node (proc_top_left) {}
  (inicio.center) ++(0, 0) node (proc_bot_left) {}
  ;

  \draw
  (proc_top_left.center) ++(\procWidth/2,0) node [below] {\Large{PicoRV32}}
  ;

  %%
  %% i CLK
  %% i RESETN
  %% o TRAP
  %%

  \draw
  (proc_top_left.center) ++ (0, -2*\textVStep) node (clkI) {}
    (clkI.center) -- ++(-\inputLineLength, 0) node [left] {clk}
  ;

  \draw
  (clkI.center) ++ (0, -\textVStep) node (resetnI) {}
    (resetnI.center) -- ++(-\inputLineLength, 0) node [left] {resetn}
  ;

  \draw
  (clkI.center) ++ (\procWidth, 0) node (trapO) {}
    (trapO.center) -- ++(\inputLineLength, 0) node [right] {trap}
  ;

  %%
  %% i MEM_READY
  %% i MEM_RDATA 32
  %%
  %% o MEM_VALID
  %% o MEM_INSTR
  %% o MEM_ADDR 32
  %% o MEM_WDATA 32
  %% o MEM_WSTRB 4
  %%
  \draw[dashed, color=black!50!white]
  (proc_top_left.center) ++ (0, -4*\textVStep) -- ++(\procWidth, 0)
  ;

  \draw[color=black!70!white]
  (proc_top_left.center) ++(\procWidth/2, -4*\textVStep) node [below] {Interfaz con Memoria}
  ;

  \draw
  (proc_top_left.center) ++ (0, -5*\textVStep) node (mem_readyI) {}
    (mem_readyI.center) -- ++(-\inputLineLength, 0) node [left] {mem\_ready}
  ;

  \draw
  (mem_readyI.center) ++ (0, -\textVStep) node (mem_rdataI) {}
    (mem_rdataI.center) -- ++(-\inputLineLength, 0) node [left] {mem\_rdata}
    (mem_rdataI.center) ++ (-\inputLineLength/2,0) node (mem_rdataI_bus_line) {}
  ;
  \busLine{mem_rdataI_bus_line}{32}

  \draw
  (mem_readyI.center) ++ (\procWidth, 0) node (mem_validO) {}
    (mem_validO.center) -- ++(\inputLineLength, 0) node [right] {mem\_valid}
  ;

  \draw
  (mem_validO.center) ++ (0, -\textVStep) node (mem_instrO) {}
    (mem_instrO.center) -- ++(\inputLineLength, 0) node [right] {mem\_instr}
  ;

  \draw
  (mem_instrO.center) ++ (0, -\textVStep) node (mem_addrO) {}
    (mem_addrO.center) -- ++(\inputLineLength, 0) node [right] {mem\_addr}
    (mem_addrO.center) ++ (\inputLineLength/2,0) node (mem_addrO_bus_line) {}
  ;
  \busLine{mem_addrO_bus_line}{32}

  \draw
  (mem_addrO.center) ++ (0, -\textVStep) node (mem_wdataO) {}
    (mem_wdataO.center) -- ++(\inputLineLength, 0) node [right] {mem\_wdata}
    (mem_wdataO.center) ++ (\inputLineLength/2,0) node (mem_wdataO_bus_line) {}
  ;
  \busLine{mem_wdataO_bus_line}{32}

  \draw
  (mem_wdataO.center) ++ (0, -\textVStep) node (mem_wstrbO) {}
    (mem_wstrbO.center) -- ++(\inputLineLength, 0) node [right] {mem\_wstrb}
    (mem_wstrbO.center) ++ (\inputLineLength/2,0) node (mem_wstrbO_bus_line) {}
  ;
  \busLine{mem_wstrbO_bus_line}{4}

  %%
  %% o MEM_LA_READ
  %% o MEM_LA_WRITE
  %% o MEM_LA_ADDR 32
  %% o MEM_LA_WDATA 32
  %% o MEM_LA_WSTRB 4
  %%
  \draw[dashed, color=black!50!white]
  (proc_top_left.center) ++ (0, -10*\textVStep) -- ++(\procWidth, 0)
  ;

  \draw[color=black!70!white]
  (proc_top_left.center) ++(\procWidth/2, -10*\textVStep) node [below] {Interfaz \textit{Look-Ahead}}
  (proc_top_left.center) ++(\procWidth/2, -10.5*\textVStep) node [below] {con Memoria}
  ;

  \draw
  (proc_top_left.center) ++ (\procWidth, -11*\textVStep) node (mem_la_readO) {}
    (mem_la_readO.center) -- ++(\inputLineLength, 0) node [right] {mem\_la\_read}
  ;

  \draw
  (mem_la_readO.center) ++ (0, -\textVStep) node (mem_la_writeO) {}
    (mem_la_writeO.center) -- ++(\inputLineLength, 0) node [right] {mem\_la\_write}
  ;

  \draw
  (mem_la_writeO.center) ++ (0, -\textVStep) node (mem_la_addrO) {}
    (mem_la_addrO.center) -- ++(\inputLineLength, 0) node [right] {mem\_la\_addr}
    (mem_la_addrO.center) ++ (\inputLineLength/2,0) node (mem_la_addrO_bus_line) {}
  ;
  \busLine{mem_la_addrO_bus_line}{32}

  \draw
  (mem_la_addrO.center) ++ (0, -\textVStep) node (mem_la_wdataO) {}
    (mem_la_wdataO.center) -- ++(\inputLineLength, 0) node [right] {mem\_la\_wdata}
    (mem_la_wdataO.center) ++ (\inputLineLength/2,0) node (mem_la_wdataO_bus_line) {}
  ;
  \busLine{mem_la_wdataO_bus_line}{32}

  \draw
  (mem_la_wdataO.center) ++ (0, -\textVStep) node (mem_la_wstrbO) {}
    (mem_la_wstrbO.center) -- ++(\inputLineLength, 0) node [right] {mem\_la\_wstrb}
    (mem_la_wstrbO.center) ++ (\inputLineLength/2,0) node (mem_la_wstrbO_bus_line) {}
  ;
  \busLine{mem_la_wstrbO_bus_line}{4}

  %%
  %% i PCPI_WR
  %% i PCPI_RD 32
  %% i PCPI_WAIT
  %% i PCPI_READY
  %%
  %% o PCPI_VALID
  %% o PCPI_INSN 32
  %% o PCPI_RS1 32
  %% o PCPI_RS2 32
  %%
  \draw[dashed, color=black!50!white]
  (proc_top_left.center) ++ (0, -16*\textVStep) -- ++(\procWidth, 0)
  ;

  \draw[color=black!70!white]
  (proc_top_left.center) ++(\procWidth/2, -16*\textVStep) node [below] {Interfaz con el}
  (proc_top_left.center) ++(\procWidth/2, -16.5*\textVStep) node [below] {\textit{Pico Co-Processor}}
  ;

  \draw
  (proc_top_left.center) ++ (0, -17*\textVStep) node (pcpi_wrI) {}
    (pcpi_wrI.center) -- ++(-\inputLineLength, 0) node [left] {pcpi\_wr}
  ;

  \draw
  (pcpi_wrI.center) ++ (0, -\textVStep) node (pcpi_rdI) {}
    (pcpi_rdI.center) -- ++(-\inputLineLength, 0) node [left] {pcpi\_rd}
    (pcpi_rdI.center) ++ (-\inputLineLength/2,0) node (pcpi_rdI_bus_line) {}
  ;
  \busLine{pcpi_rdI_bus_line}{32}

  \draw
  (pcpi_rdI.center) ++ (0, -\textVStep) node (pcpi_waitI) {}
    (pcpi_waitI.center) -- ++(-\inputLineLength, 0) node [left] {pcpi\_wait}
  ;

  \draw
  (pcpi_waitI.center) ++ (0, -\textVStep) node (pcpi_readyI) {}
    (pcpi_readyI.center) -- ++(-\inputLineLength, 0) node [left] {pcpi\_ready}
  ;

  \draw
  (pcpi_wrI.center) ++ (\procWidth, 0) node (pcpi_validO) {}
    (pcpi_validO.center) -- ++(\inputLineLength, 0) node [right] {pcpi\_valid}
  ;

  \draw
  (pcpi_validO.center) ++ (0, -\textVStep) node (pcpi_insnO) {}
    (pcpi_insnO.center) -- ++(\inputLineLength, 0) node [right] {pcpi\_insn}
    (pcpi_insnO.center) ++ (\inputLineLength/2,0) node (pcpi_insnO_bus_line) {}
  ;
  \busLine{pcpi_insnO_bus_line}{32}

  \draw
  (pcpi_insnO.center) ++ (0, -\textVStep) node (pcpi_rs1O) {}
    (pcpi_rs1O.center) -- ++(\inputLineLength, 0) node [right] {pcpi\_rs1}
    (pcpi_rs1O.center) ++ (\inputLineLength/2,0) node (pcpi_rs1O_bus_line) {}
  ;
  \busLine{pcpi_rs1O_bus_line}{32}

  \draw
  (pcpi_rs1O.center) ++ (0, -\textVStep) node (pcpi_rs2O) {}
    (pcpi_rs2O.center) -- ++(\inputLineLength, 0) node [right] {pcpi\_rs2}
    (pcpi_rs2O.center) ++ (\inputLineLength/2,0) node (pcpi_rs2O_bus_line) {}
  ;
  \busLine{pcpi_rs2O_bus_line}{32}

  %%
  %% i IRQ 32
  %% o EOI 32
  %%
  \draw[dashed, color=black!50!white]
  (proc_top_left.center) ++ (0, -21*\textVStep) -- ++(\procWidth, 0)
  ;

  \draw[color=black!70!white]
  (proc_top_left.center) ++(\procWidth/2, -21*\textVStep) node [below] {Interfaz}
  (proc_top_left.center) ++(\procWidth/2, -21.5*\textVStep) node [below]
  {de Interrupciones}
  ;

  \draw
  (proc_top_left.center) ++ (0, -22*\textVStep) node (irqI) {}
    (irqI.center) -- ++(-\inputLineLength, 0) node [left] {irq}
    (irqI.center) ++ (-\inputLineLength/2,0) node (irqI_bus_line) {}
  ;
  \busLine{irqI_bus_line}{32}

  \draw
  (irqI.center) ++ (\procWidth, 0) node (eoiO) {}
    (eoiO.center) -- ++(\inputLineLength, 0) node [right] {eoi}
    (eoiO.center) ++ (\inputLineLength/2,0) node (eoiO_bus_line) {}
  ;
  \busLine{eoiO_bus_line}{32}
\end{tikzpicture}