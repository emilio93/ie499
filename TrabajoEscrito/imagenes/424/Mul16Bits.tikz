\begin{tikzpicture}[font=\ttfamily]
  \draw (0,0) node (mul) {} 
  ++(0,1) node [below right] {\Large Mul16Bits}  rectangle ++(16,-15);
  
  \draw[->,>=stealth, thick] (mul) ++(15, -1) node (r00) {} -- ++(0,-12) 
  node [below] {R0};
  \draw (r00) ++(0,0) node [right, rotate=90] {\footnotesize a[0]\&b[0]};
  
  % TOP RIGHT
  
  \draw (mul) ++(13.5, -1.5) node (i0j0) {} rectangle ++(1, -1);
  \draw(i0j0) ++(0.5, -.5) node {+} 
  (i0j0) ++(0.5, 0) node [below] {\tiny i=0,j=0};
  \draw[->,>=stealth] (i0j0) ++(0.25, .5) node (i001) {} -- ++(0,-.5);
  \draw (i001) ++(0,0) node [right, rotate=90] {\footnotesize a[1]\&b[0]};
  \draw[->,>=stealth] (i0j0) ++(0.75, .5) node (i002) {} -- ++(0,-.5);
  \draw (i002) ++(0,0) node [right, rotate=90] {\footnotesize a[0]\&b[1]};
  \draw[->,>=stealth, color=red!50!yellow, thick] (i0j0) ++(0.5, -1) node (pr00) {} -- ++(0,-10.5)
  node (r1) {};
  \draw (r1) node [below] {R1};
  \draw[->,>=stealth, color=red!50!blue, thick] (i0j0) ++(0, -0.5) node (c00) {} -- ++(-.5,0);
  
  \draw (mul) ++(12, -1.5) node (i1j0) {} rectangle ++(1, -1);
  \draw(i1j0) ++(0.5, -.5) node {+} 
  (i1j0) ++(0.5, 0) node [below] {\tiny i=1,j=0};
  \draw[->,>=stealth] (i1j0) ++(0.25, .5) node (i101) {} -- ++(0,-.5);
  \draw (i101) ++(0,0) node [right, rotate=90] {\footnotesize a[1]\&b[1]};
  \draw[->,>=stealth] (i1j0) ++(0.75, .5) node (i102) {} -- ++(0,-.5);
  \draw (i102) ++(0,0) node [right, rotate=90] {\footnotesize a[0]\&b[2]};
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1j0) ++(0.5, -1) node (pr10) {} -- ++(0.25,-.25) -- ++(0,-1.25);
  \draw[->,>=stealth, color=red!50!blue, thick] (i1j0) ++(0, -0.5) node (c10) {} -- ++(-.5,0);
  
  \draw (mul) ++(12, -4) node (i0j1) {} rectangle ++(1, -1);
  \draw(i0j1) ++(0.5, -.5) node {+} 
  (i0j1) ++(0.5, 0) node [below] {\tiny i=0,j=1};
  \draw[->,>=stealth] (i0j1) ++(0.25, .5) node (i011) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i0j1) ++(0.5, -1) node (pr01) {} -- ++(0,-8) node (r2) {};
  \draw (r2) node [below] {R2};
  \draw[->,>=stealth, color=red!50!blue, thick] (i0j1) ++(0, -0.5) node (c01) {} -- ++(-.5,0);
  \draw (i011) ++(0,-0.5) node [above right, rotate=90] {\tiny a[2]\&b[0]};
  
  
  \draw (mul) ++(10.5, -4) node (i1j1) {} rectangle ++(1, -1);
  \draw(i1j1) ++(0.5, -.5) node {+} 
  (i1j1) ++(0.5, 0) node [below] {\tiny i=1,j=1};
  \draw[->,>=stealth] (i1j1) ++(0.25, .5) node (i111) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1j1) ++(0.5, 1) node (i112) {} -- ++(0.25,-.25) -- ++(0,-.75);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1j1) ++(0.5, -1) node (pr11) {} -- ++(0.25,-.25) -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!blue, thick] (i1j1) ++(0, -0.5) node (c11) {} -- ++(-.5,0);
  \draw (i111) ++(0,-0.5) node [above right, rotate=90] {\tiny a[2]\&b[1]};
  
  
  % BOT LEFT
  
  \draw (mul) ++(4.5, -8.5) node (il1jl1) {} rectangle ++(1, -1);
  \draw(il1jl1) ++(0.5, -.5) node {+} 
  (il1jl1) ++(0.5, 0) node [below] {\tiny i=14j=13};
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl1) ++(1.5, -.5) node  {} -- ++(-.5,0);
  \draw[->,>=stealth] (il1jl1) ++(0.25, .5) node (i14131) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il1jl1) ++(0.5, 1) -- ++(0.25,-.25) -- ++(0,-.75);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il1jl1) ++(0.5, -1) node  {} -- ++(0.25,-.25) -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl1) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i14131) ++(0,-0.5) node [above right, rotate=90] {\tiny a[14]\&b[14]};
  
  
  \draw (mul) ++(3, -8.5) node (il0jl1) {} rectangle ++(1, -1);
  \draw(il0jl1) ++(0.5, -.5) node {+} 
  (il0jl1) ++(0.5, 0) node [below] {\tiny i=15j=13};
  \draw[->,>=stealth] (il0jl1) ++(0.25, .5) node (i15131) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!blue, thick] (il0jl1) ++(1, 1.25) -- ++(-0.25,-.25) -- ++(0,-1);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il0jl1) ++(0.5, -1) node  {} -- ++(0.25,-.25) -- ++(0,-1.25);
  \draw[->,>=stealth, color=red!50!blue, thick] (il0jl1) ++(0, -0.5) node  {} -- ++(-.5,0) -- ++(-.25,-.25) -- ++(0,-1.75);
  \draw (i15131) ++(0,-0.5) node [above right, rotate=90] {\tiny a[14]\&b[15]};
  
  \draw (mul) ++(3, -11) node (il1jl0) {} rectangle ++(1, -1);
   \draw(il1jl0) ++(0.5, -.5) node {+} 
  (il1jl0) ++(0.5, 0) node [below] {\tiny i=14j=14};
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl0) ++(1.5, -.5) node  {} -- ++(-.5,0);
  \draw[->,>=stealth] (il1jl0) ++(0.25, .5) node (i14141) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il1jl0) ++(0.5, -1) node  {} -- ++(0,-1);
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl0) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i14141) ++(0,-0.5) node [above right, rotate=90] {\tiny a[15]\&b[15]};

  \draw (mul) ++(1.5, -11) node (il0jl0) {} rectangle ++(1, -1);
  \draw(il0jl0) ++(0.5, -.5) node {+} 
  (il0jl0) ++(0.5, 0) node [below] {\tiny i=15j=14};
  \draw[->,>=stealth] (il0jl0) ++(0.25, .5) node (i15131) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il0jl0) ++(0.5, -1) node  {} -- ++(0,-1);
  \draw[->,>=stealth, color=red!50!blue, thick] (il0jl0) ++(0, -0.5) node  {} -- ++(-.5,0) -- ++(-.25,-.25) -- ++(0, -1.25);
  \draw (i15131) ++(0,-0.5) node [above right, rotate=90] {\tiny a[15]\&b[15]};
  
  % bot right
  
  \draw (mul) ++(10, -8.5) node (i1jl1) {} rectangle ++(1, -1);
  \draw(i1jl1) ++(0.5, -.5) node {+} 
  (i1jl1) ++(0.5, 0) node [below] {\tiny i=0,j=13};
  \draw[->,>=stealth] (i1jl1) ++(0.25, .5) node (i0131) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1jl1) ++(0.5, 1) -- ++(0.25,-.25) -- ++(0,-.75);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1jl1) ++(0.5, -1) node  {} -- ++(0,-3.5)
  node (r14) {};
  \draw  (r14) node [below] {R14};
  \draw[->,>=stealth, color=red!50!blue, thick] (i1jl1) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i0131) ++(0,-0.5) node [above right, rotate=90] {\tiny a[14]\&b[0]};
  
  \draw (mul) ++(8.5, -8.5) node (i1jl1) {} rectangle ++(1, -1);
  \draw(i1jl1) ++(0.5, -.5) node {+} 
  (i1jl1) ++(0.5, 0) node [below] {\tiny i=1,j=13};
  \draw[->,>=stealth] (i1jl1) ++(0.25, .5) node (i1131) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1jl1) ++(0.5, 1) -- ++(0.25,-.25) -- ++(0,-.75);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1jl1) ++(0.5, -1) node  {} -- ++(0.25,-.25) -- ++(0,-1.25);
  \draw[->,>=stealth, color=red!50!blue, thick] (i1jl1) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i1131) ++(0,-0.5) node [above right, rotate=90] {\tiny a[14]\&b[1]};
  
  \draw (mul) ++(8.5, -11) node (i0jl0) {} rectangle ++(1, -1);
   \draw(i0jl0) ++(0.5, -.5) node {+} 
  (i0jl0) ++(0.5, 0) node [below] {\tiny i=1,j=14};
  \draw[->,>=stealth] (i0jl0) ++(0.25, .5) node (i0141) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i0jl0) ++(0.5, -1) node  {} -- ++(0,-1)
  node (r15) {};
  \draw (r15) node [below] {R15};
  \draw[->,>=stealth, color=red!50!blue, thick] (i0jl0) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i0141) ++(0,-0.5) node [above right, rotate=90] {\tiny a[15]\&b[0]};

  \draw (mul) ++(7, -11) node (i1jl0) {} rectangle ++(1, -1);
  \draw(i1jl0) ++(0.5, -.5) node {+} 
  (i1jl0) ++(0.5, 0) node [below] {\tiny i=1,j=14};
  \draw[->,>=stealth] (i1jl0) ++(0.25, .5) node (i1141) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1jl0) ++(0.5, 1) -- ++(0.25,-.25) -- ++(0,-.75);
  \draw[->,>=stealth, color=red!50!yellow, thick] (i1jl0) ++(0.5, -1) node  {} -- ++(0,-1);
  \draw[->,>=stealth, color=red!50!blue, thick] (i1jl0) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i1141) ++(0,-0.5) node [above right, rotate=90] {\tiny a[15]\&b[1]};
  
  
  
  \draw (mul) ++(8, -1.5) node (il1jl1) {} rectangle ++(1, -1);
  \draw(il1jl1) ++(0.5, -.5) node {+} 
  (il1jl1) ++(0.5, 0) node [below] {\tiny i=14,j=0};
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl1) ++(1.5, -.5) node  {} -- ++(-.5,0);
  \draw[->,>=stealth] (il1jl1) ++(0.25, .5) node (i1411) {} -- ++(0,-.5);
  \draw (i1411) ++(0,0) node [right, rotate=90] {\footnotesize a[1]\&b[14]};
  \draw[->,>=stealth] (il1jl1) ++(0.75, .5) node (i1412) {} -- ++(0,-.5);
  \draw (i1412) ++(0,0) node [right, rotate=90] {\footnotesize a[0]\&b[15]};
  \draw[->,>=stealth, color=red!50!yellow, thick] (il1jl1) ++(0.5, -1) node  {} -- ++(0.25,-.25) -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl1) ++(0, -0.5) node  {} -- ++(-.5,0);
  
  
  \draw (mul) ++(6.5, -1.5) node (il0jl1) {} rectangle ++(1, -1);
  \draw(il0jl1) ++(0.5, -.5) node {+} 
  (il0jl1) ++(0.5, 0) node [below] {\tiny i=15,j=0};
  \draw[->,>=stealth] (il0jl1) ++(0.25, .5) node (i1511) {} -- ++(0,-.5);
  \draw[->,>=stealth] (il0jl1) ++(0.75, .5) node  (i1512) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il0jl1) ++(0.5, -1) node {} -- ++(0.25,-.25) -- ++(0,-1.25);
  \draw (i1511) ++(0,0) node [right, rotate=90] {\footnotesize a[1]\&b[15]};
  \draw (i1512) ++(0,0) node [right, rotate=90] {\footnotesize 0};
  \draw[->,>=stealth, color=red!50!blue, thick] (il0jl1) ++(0, -0.5) node  {} -- ++(-.5,0) -- ++(-.25,-.25) -- ++(0,-1.75);
  
  \draw (mul) ++(6.5, -4) node (il1jl0) {} rectangle ++(1, -1);
   \draw(il1jl0) ++(0.5, -.5) node {+} 
  (il1jl0) ++(0.5, 0) node [below] {\tiny i=14,j=1};
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl0) ++(1.5, -.5) node  {} -- ++(-.5,0);
  \draw[->,>=stealth] (il1jl0) ++(0.25, .5) node (i1421) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il1jl0) ++(0.5, -1) node  {} -- ++(0,-1);
  \draw[->,>=stealth, color=red!50!blue, thick] (il1jl0) ++(0, -0.5) node  {} -- ++(-.5,0);
  \draw (i1421) ++(0,-0.5) node [above right, rotate=90] {\tiny a[2]\&b[14]};

  \draw (mul) ++(5, -4) node (il0jl0) {} rectangle ++(1, -1);
  \draw(il0jl0) ++(0.5, -.5) node {+} 
  (il0jl0) ++(0.5, 0) node [below] {\tiny i=15,j=1};
  \draw[->,>=stealth] (il0jl0) ++(0.25, .5) node (i1521) {} -- ++(0,-.5);
  \draw[->,>=stealth, color=red!50!yellow, thick] (il0jl0) ++(0.5, -1) node  {} -- ++(0,-1);
  \draw (i1521) ++(0,-0.5) node [above right, rotate=90] {\tiny a[2]\&b[15]};
  \draw[->,>=stealth, color=red!50!blue, thick] (il0jl0) ++(0, -0.5) node  {} -- ++(-.5,0) -- ++(-.25,-.25) -- ++(0, -1.25);

  
  \draw (mul) ++(10.5,-2) node {\large$\cdot\cdot\cdot$};
  \draw (mul) ++(9,-4.5) node {\large$\cdot\cdot\cdot$};
  
  \draw (mul) ++(10.5,-6.75) node [rotate=45] {\large$\cdot\cdot\cdot$};
  \draw (mul) ++(8,-6.75) node [rotate=45] {\large$\cdot\cdot\cdot$};
  \draw (mul) ++(5,-6.75) node [rotate=45] {\large$\cdot\cdot\cdot$};
  
  \draw (mul) ++(7,-9) node {\large$\cdot\cdot\cdot$};
  \draw (mul) ++(5.5,-11.5) node {\large$\cdot\cdot\cdot$};
  
  \draw (mul) ++(5.5,-12.75) node {\large$\cdot\cdot\cdot$};
  \draw (mul) ++(11.5,-12.75) node {\large$\cdot\cdot\cdot$};
  
  
\end{tikzpicture}
