\begin{tikzpicture}[font=\ttfamily\footnotesize]

  \draw (0, 4) node (inputtop) {} ++(0,-4) node (inputbot) {};
  
  %
  % 0
  %
  \draw[->,>=latex] (inputtop.center) ++(0,0) node [above] {\footnotesize 0} 
  -> ++(0,-6) node [below] {\footnotesize 0x0};
  
  %
  % A
  %
  \draw[->,>=latex] (inputtop.center) ++(1,0) node [above] {\footnotesize A} 
  -> ++(0, -6)  node [below] {\footnotesize 0x1};

  %
  % A<<1
  %
  \draw[->,>=latex] (inputtop.center) ++(2, 0) node [above] {\footnotesize A$<<$1} 
  -> ++(0, -6)  node [below] {\footnotesize 0x2};

  %
  % A<<1 + A
  %
  \filldraw (inputtop.center) ++(2,-.25) circle (0.04cm);
  \draw[->,>=latex] (inputtop.center) ++(2, -0.25) -- ++(0.75,0) -- ++(0, -.25);
  \draw[->,>=latex] (inputtop.center) ++(3.25, 0) node [above] {\footnotesize A} -> ++(0, -.5 );
  \draw (inputtop.center) ++(2.5,-0.5) rectangle ++(1,-1) ++(-0.5,0.5) node {\Large +};
  \draw[->,>=latex] (inputtop.center) ++(3, -1.5) 
  -> ++(0, -4.5)  node [below] {\footnotesize 0x3};
  
  %
  % A<<2
  %
  \draw[->,>=latex] (inputtop.center) ++(4, 0) node [above] {\footnotesize A$<<$2} 
  -> ++(0, -6)  node [below] {\footnotesize 0x4};

  %
  % A<<2+A
  %
  \filldraw (inputtop.center) ++(4,-.25) circle (0.04cm);
  \draw[->,>=latex] (inputtop.center) ++(4, -0.25) -- ++(0.75,0) -- ++(0, -.25);
  \draw[->,>=latex] (inputtop.center) ++(5.25, 0) node [above] {A} -> ++(0, -.5 );
  \draw (inputtop.center) ++(4.5,-0.5) rectangle ++(1,-1) ++(-0.5,0.5) node {\Large +};
  \draw[->,>=latex] (inputtop.center) ++(5, -1.5) 
  -> ++(0, -4.5)  node [below] {\footnotesize 0x5};
  
  %
  % A<<2 + A<<1
  %
  \filldraw (inputtop.center) 
            ++(4,-2) node (ashift2_1) {} 
            circle (0.04cm);
  \draw[->,>=latex] (ashift2_1.center)
                    -- ++(1.9,0) node (ashift2_2) {} -- ++(0, -.75);
  \filldraw (inputtop.center) 
             ++(2,-1.75) node (ashift1_1) {}
             circle (0.04cm);
  \draw[->,>=latex] (ashift1_1.center) 
                    -- ++(4.1, 0) node (ashift1_2) {} -- ++(0, -1);
  \draw (inputtop.center) ++(5.65,-2.75) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0,-0.35) node (h_6sum) {};
  \draw[->,>=latex] (h_6sum.center) -> ++(0, -2.55)  node [below] {\footnotesize 0x6};
  
  %
  % A<<2 + A<<1 + A
  %
  \filldraw (h_6sum.center) ++(0,-.3) circle (0.04cm);
  \draw[->,>=latex] (h_6sum.center) ++(0,-.3) -- ++(.9, 0) -- ++(0, -.5);
  \draw[->,>=latex] (inputtop.center) ++(7.1, 0) node [above] {A} -> ++(0, -4.25 );
  \draw (inputtop.center) ++(6.65,-4.24) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0,-.35) node (h_7sum) {};
  \draw[->,>=latex] (h_7sum.center) -> ++(0, -1.05)  node [below] {\footnotesize 0x7};
  
  %
  % A<<3
  %
  \draw[->,>=latex] (inputtop.center) ++(8, 0) node [above] {\footnotesize A$<<$3}
  -> ++(0, -6)  node [below] {\footnotesize 0x8};
  
  %
  % A<<3+A
  %
  \filldraw (inputtop.center) ++(8,-.25) circle (0.04cm);
  \draw[->,>=latex] (inputtop.center) ++(8, -0.25) -- ++(0.75,0) -- ++(0, -.25);
  \draw[->,>=latex] (inputtop.center) ++(9.25, 0) node [above] {A} -> ++(0, -.5 );
  \draw (inputtop.center) ++(8.5,-0.5) rectangle ++(1,-1) ++(-0.5,0.5) node {\Large +};
  \draw[->,>=latex] (inputtop.center) ++(9, -1.5) 
  -> ++(0, -4.5)  node [below] {\footnotesize 0x9};
  
  \filldraw (inputtop.center) ++(8,-2.25) node (ashift3_1) {} circle (0.04cm);
  \draw[->,>=latex] (ashift3_1.center) -- ++(1.9, 0) node (ashift3_2) {} -- ++(0, -.5);
  \filldraw (ashift1_2.center) circle (0.04cm);
  \draw[->,>=latex] (ashift1_2.center) -- ++(4,0) node (ashift1_3) {} -- ++(0, -1);
  \draw (inputtop.center) ++(9.65,-2.75) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0, -.35) node (h_asum) {};
  \draw[->,>=latex] (h_asum.center) -> ++(0,-2.55)  node [below] {\footnotesize 0xA};
  
  
  \filldraw (h_asum.center) ++(0,-.3) circle (0.04cm);
  \draw[->,>=latex] (h_asum.center) ++(0,-.3) -- ++(.9, 0) -- ++(0, -.5);
  \draw[->,>=latex] (inputtop.center) ++(11.1, 0) node [above] {A} -> ++(0, -4.25 );
  \draw (inputtop.center) ++(10.65,-4.25) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0,-.35) node (h_bsum) {};
  \draw[->,>=latex] (h_bsum.center) -> ++(0, -1.05)  node [below] {\footnotesize 0xB};
  
  
  \filldraw (ashift3_2.center) circle (0.04cm);
  \draw[->,>=latex] (ashift3_2.center) -- ++(2, 0) node (ashift3_2) {} -- ++(0, -.5);
  \filldraw (ashift2_2.center) circle (0.04cm);
  \draw[->,>=latex] (ashift2_2.center) -- ++(6.2,0) node (ashift2_3) {} -- ++(0, -.75);
  \draw (inputtop.center) ++(11.65,-2.75) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0, -.35) node (h_csum) {};
  \draw[->,>=latex] (h_csum.center) -> ++(0,-2.55)  node [below] {\footnotesize 0xC};
  
  
  \filldraw (h_csum.center) ++(0,-.3) circle (0.04cm);
  \draw[->,>=latex] (h_csum.center) ++(0,-.3) -- ++(.9, 0) -- ++(0, -.5);
  \draw[->,>=latex] (inputtop.center) ++(13.1, 0) node [above] {A} -> ++(0, -4.25 );
  \draw (inputtop.center) ++(12.65,-4.25) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0,-.35) node (h_dsum) {};
  \draw[->,>=latex] (h_dsum.center) -> ++(0, -1.05)  node [below] {\footnotesize 0xD};
  
  \filldraw (ashift1_3.center) circle (0.04cm);
  \draw[->,>=latex] (ashift1_3.center) -- ++(4.1, 0) node (ashift1_5) {} -- ++(0, -1);
    \filldraw (ashift2_3.center) circle (0.04cm);
  \draw[->,>=latex] (ashift2_3.center) -- ++(1.9, 0) node (ashift1_5) {} -- ++(0, -.75);
      \filldraw (ashift3_2.center) circle (0.04cm);
  \draw[->,>=latex] (ashift3_2.center) -- ++(1.9, 0) node (ashift1_5) {} -- ++(0, -.5);
  \draw (inputtop.center) ++(13.65,-2.75) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0, -.35) node (h_esum) {};
  \draw[->,>=latex] (h_esum.center) -> ++(0,-2.55)  node [below] {\footnotesize 0xE};
  
    \filldraw (h_esum.center) ++(0,-.3) circle (0.04cm);
  \draw[->,>=latex] (h_esum.center) ++(0,-.3) -- ++(.9, 0) -- ++(0, -.5);
  \draw[->,>=latex] (inputtop.center) ++(15.1, 0) node [above] {A} -> ++(0, -4.25 );
  \draw (inputtop.center) ++(14.65,-4.25) rectangle ++(.7,-.7) ++(-0.35,0.35) node {\Large +} ++(0,-.35) node (h_fsum) {};
  \draw[->,>=latex] (h_fsum.center) -> ++(0, -1.05)  node [below] {\footnotesize 0xF};
  
  \draw (-0.5,-2) 
  -- ++(16,0) 
  -- ++(-1, -2) 
  -- ++(-14, 0) 
  -- ++(-1, 2) 
  ++(8,-1) node {\Large MUX};
  
  \draw[->,>=latex] (-1.25, -1) node [left] {A(N bits)} 
  -> ++(.5, 0);
  
  \draw[->,>=latex] (-1.25, -3) node [left] {B(4 bits)} 
  -> ++(1.25, 0);
  
  \draw[->,>=latex] (7.5, -4) 
  -> ++(0, -1) node [below] {A*B};
  
  \draw (-.75, 4.5) node [above right] {\Large LUT\_4Bits\_Helper} 
  rectangle ++(16.5, -8.75);
\end{tikzpicture}