\begin{tikzpicture}[font=\ttfamily]
  \draw
  (0,0) node (rom) {}
  rectangle ++(2,11) ++(-1,-5.5) 
  node {ROM}

  (rom) ++(3.5,0)
  node (currentIP) {}
  rectangle ++(2.5,2) ++(-1.25,-1) ++(0,6pt)
  node {Current} ++(0,-2pt) node [below] {IP}

  (currentIP) ++(2.5,0)
  node (branchLogic) {} 
  rectangle ++(2.5,2) ++(-1.25,-1) ++(0,6pt)
  node {Branch} ++(0,-2pt) node [below] {Logic}

  (branchLogic) ++(6,0)
  node (alu) {}
  rectangle ++(2,11) ++(-1,-5.5)
  node {ALU}

  (currentIP) ++(-0.5,2.75)
  node (ram) {}
  rectangle ++(6,3) ++(-3,-1.5) ++(0,6pt)
  node {Data RAM} ++(0,-2pt) node [below] {(Dual read channel)}

  (rom) ++(2,9.9)
  node (instruction) {}
  -- ++(3.8,0) -- ++(0,0.3) -- ++(1.2,-0.7)
  -- ++(-1.2,-0.7) -- ++(0,0.3) -- ++(-3.8,0)
  ++(2.3,0.4) node {Instruction}

  (instruction) ++(5,1.1)
  rectangle ++(2,-1)
  ++(-1,0.5) node (ffd1) {FFD} ++(1,-0.5)
  rectangle ++(-2,-1)
  ++(1,0.5) node (ffd2) {FFD} ++(-1,-0.5)
  rectangle ++(2,-1)
  ++(-1,0.5) node (ffd3) {FFD} ++(1,-0.5)
  ;
  \filldraw 
  (currentIP) ++(0,1) -- ++(-1.5,0) 
  -- ++(0.2,0.1) -- ++(0,-0.2) -- ++(-0.2,0.1)
  ++(0.75,0) node [above] {IP}
  ;
  \filldraw 
  (alu) ++(0,1) -- ++(-3.5,0) 
  -- ++(0.2,0.1) -- ++(0,-0.2) -- ++(-0.2,0.1)
  ++(1.75,0) node [above] {Branch Taken}
  ;

  \filldraw 
  (ram) ++(6,0.15) ++(3,0)  -- ++(-3,0) 
  -- ++(0.2,0.1) -- ++(0,-0.2) -- ++(-0.2,0.1)
  ++(1.5,0) node [below] {Result}
  ;
  \filldraw 
  (ram) ++(6,1.05) -- ++(3,0) ++(-3,0) 
  -- ++(0.2,0.1) -- ++(0,-0.2) -- ++(-0.2,0.1)
  ++(1.5,0) node [below] {WriteEn}
  ;
  \filldraw 
  (ram) ++(6,1.95) -- ++(3,0) 
  -- ++(-0.2,0.1) -- ++(0,-0.2) -- ++(0.2,0.1)
  ++(-1.5,0) node [below] {Data1}
  ;
  \filldraw 
  (ram) ++(6,2.85) -- ++(3,0) 
  -- ++(-0.2,0.1) -- ++(0,-0.2) -- ++(0.2,0.1)
  ++(-1.5,0) node [below] {Data0}
  ;

  \filldraw 
  (ffd1) ++(1,0) -- ++(3,0) 
  -- ++(-0.2,0.1) -- ++(0,-0.2) -- ++(0.2,0.1)
  ++(-1.5,0) node [below] {Operation}
  ;
  \filldraw 
  (ffd2) ++(1,0) -- ++(3,0) 
  -- ++(-0.2,0.1) -- ++(0,-0.2) -- ++(0.2,0.1)
  ++(-1.5,0) node [below] {ImmediateValue}
  ;
  \filldraw 
  (ffd3) ++(1,0) -- ++(3,0) 
  -- ++(-0.2,0.1) -- ++(0,-0.2) -- ++(0.2,0.1)
  ++(-1.5,0) node [below] {Destination}
  ;

  \filldraw 
  (instruction) ++(1.5,-.8)  -- ++(0,-1.65)
  node [above, rotate=-90] {SourceAddr0} -- ++(0, -1.7)
  -- ++(0.1,0.2) -- ++(-0.2,0) -- ++(0.1,-0.2)
  ;
  \filldraw 
  (instruction) ++(3,-.8) -- ++(0,-1.65)
  node [above, rotate=-90] {SourceAddr1} -- ++(0, -1.7)
  -- ++(0.1,0.2) -- ++(-0.2,0) -- ++(0.1,-0.2)
  ;
\end{tikzpicture}