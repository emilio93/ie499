#!/bin/bash

PROY_NAME=proyecto
PRES_NAME=presentacion
PRES_FILE=${PRES_NAME}.tex
PROY_FILE=${PROY_NAME}.tex
INTERACTION=nonstopmode
PROY_OUTPUT_DIR=output
PDFLATEX_OPTS="--shell-escape -interaction=${INTERACTION} -file-line-error --output-directory ${PROY_OUTPUT_DIR}"

curDir="$( pwd )"
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# cd ${scriptDir}
# cd Presentacion
# mkdir -p ${PROY_OUTPUT_DIR}
# mkdir -p logs

# echo "**************"
# echo "Presentación"
# pdflatex ${PRES_FILE} ${PDFLATEX_OPTS} > logs/${PRES_NAME}.log
# pdflatex ${PRES_FILE} ${PDFLATEX_OPTS} > logs/${PRES_NAME}.log
# cat presentacion.log | grep -in ".*:[0-9]*:.*\|warning"
# echo "*******************"
# echo "Presentación done"
# echo "*******************"

# cp ${PRES_NAME}.pdf ../${PRES_NAME}.pdf


#
# PROYECTO
#


cd ${scriptDir}
cd TrabajoEscrito
mkdir -p ${PROY_OUTPUT_DIR}
mkdir -p logs

echo "**************"
echo "Latex 1st pass"
pdflatex ${PDFLATEX_OPTS} ${PROY_FILE} > logs/pdflatex1.log
echo "Latex 1st pass done"
echo "*******************"

errorswarnings1=$(cat logs/pdflatex1.log | \
                  grep -in ".*:[0-9]*:.*\|warning" | wc -l)

cp ${PROY_OUTPUT_DIR}/${PROY_NAME}.pdf ../${PROY_NAME}.pdf

if [ "$errorswarnings1" -le "2" ]; then
  echo "nothing to add"
  exit 42 &>/dev/null
fi

checksum1=$(sha256sum ${PROY_OUTPUT_DIR}/${PROY_NAME}.pdf | \
            grep -io "[0-9a-f]*" | head -1)

echo "**********"
echo "biber pass"
# echo "**********"
biber --input-directory $PROY_OUTPUT_DIR \
      --output-directory $PROY_OUTPUT_DIR $PROY_NAME \
      > logs/bieber1.log
# cat logs/bieber1.log | grep -in "warn"
# echo "**************"
echo "biber pass done"
echo "**************"

echo "**************"
echo "Latex 2nd pass"
echo "**************"
pdflatex ${PDFLATEX_OPTS} ${PROY_FILE} > logs/pdflatex2.log
cat logs/pdflatex2.log | grep -in ".*:[0-9]*:.*\|warning"
echo "*******************"
echo "Latex 2nd pass done"
echo "*******************"

checksum2=$(sha256sum ${PROY_OUTPUT_DIR}/${PROY_NAME}.pdf | \
            grep -io "[0-9a-f]*" | head -1)

if [ "${checksum1}" == "${checksum2}" ]; then
  echo "Sin Cambios."
else
  cp ${PROY_OUTPUT_DIR}/${PROY_NAME}.pdf ../${PROY_NAME}.pdf
fi


#
# LABORATORIOS
#

cd ${scriptDir}
cd TrabajoEscrito
mkdir -p ${PROY_OUTPUT_DIR}
mkdir -p logs

echo "**************"
echo "Laboratorios"
echo "**************"

for i in 1 2 3 4
do

  echo "Lab $i"
  old="\input{file}"
  new="\input{laboratorios/laboratorio${i}.tex}"

  sed -i "s:${old}:${new}:g" laboratorio.tex

  pdflatex ${PDFLATEX_OPTS} -jobname=${i} laboratorio.tex > logs/laboratorio${i}.log
  pdflatex ${PDFLATEX_OPTS} -jobname=${i} laboratorio.tex > logs/laboratorio${i}.log

  sed -i "s:${new}:${old}:g" laboratorio.tex

  cp ${PROY_OUTPUT_DIR}/${i}.pdf ../laboratorio${i}.pdf
  echo "Lab $i done"
done



cd ${curDir}
